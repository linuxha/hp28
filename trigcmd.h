/****************************************************************

	Functions used for implementing user trigonometric commands

0.0	hjp	89-12-03

	initial version
	SIN, COS, TAN, ASIN, ACOS, ATAN extracted from ArithCmd.

****************************************************************/

#ifndef I_trigcmd

	#define I_trigcmd

	void	c_acos	(void);
	void	c_asin	(void);
	void	c_atan	(void);
	void	c_cos	(void);
	void	c_sin	(void);
	void	c_tan	(void);

#endif
