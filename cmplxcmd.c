/****************************************************************

Module:
	CmplxCmd

Description:
	Commands related to complex numbers (The CMPLX menu on HP28C)

Modification history:

	0.0	hjp	89-12-03

		initial version
		R->C, C->R, RE, IM extracted from ArithCmd.

****************************************************************/

#include "cmplxcmd.h"
#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "intcmd.h"
#include "stackcmd.h"

/*
	R->C	convert two reals into a complex number

	x	y	->	(x, y)

	REAL	REAL	->	COMPLEX
*/

void	c_r_c (void)
{
	realobj 	* a, * b;
	complexobj	* c;

	if (! stack && ! stack->next) {
		lerror ("R->C", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL && (b = stack->next->obj)->id == REAL) {
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("R->C", ERR_NOMEM);
			return;
		}
		c->val.x = b->val; c->val.y = a->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		lerror ("R->C", ERR_WRTYPE);
	}
}


/*
	C->R	convert a complex to two reals

	(x, y)	->	x	y

	COMPLEX ->	REAL	REAL
*/

void	c_c_r (void)
{
	realobj 	* a, * b;
	complexobj	* c;

	if (! stack) {
		lerror ("C->R", ERR_2FEWARG);
		return;
	}

	if ((c = stack->obj)->id == COMPLEX) {

		if (!(a = mallocobj (REAL)))
		{
			lerror ("R->C", ERR_NOMEM);
			return;
		}
		a->val = c->val.x;
		if (!(b = mallocobj (REAL)))
		{
			a->link ++;		/* set link to 1, so that destroy will really free a	*/
			destroy (a, 1);
			lerror ("R->C", ERR_NOMEM);
			return;
		}
		b->val = c->val.y;
		c_drop ();
		push (a);
		push (b);
	} else {
		lerror ("R->C", ERR_WRTYPE);
	}
}


/*
	RE	real part of complex number

	(x, y)	->	x

	COMPLEX ->	REAL
*/

void	c_re (void)
{
	realobj 	* a;
	complexobj	* c;

	if (! stack) {
		lerror ("RE", ERR_2FEWARG);
		return;
	}

	if ((c = stack->obj)->id == COMPLEX) {

		if (!(a = mallocobj (REAL)))
		{
			lerror ("RE", ERR_NOMEM);
			return;
		}
		a->val = c->val.x;
		c_drop ();
		push (a);
	} else {
		lerror ("RE", ERR_WRTYPE);
	}
}


/*
	IM	imaginary part of complex number

	(x, y)	->	y

	COMPLEX ->	REAL
*/

void	c_im (void)
{
	realobj 	* a;
	complexobj	* c;

	if (! stack) {
		lerror ("IM", ERR_2FEWARG);
		return;
	}

	if ((c = stack->obj)->id == COMPLEX) {

		if (!(a = mallocobj (REAL)))
		{
			lerror ("IM", ERR_NOMEM);
			return;
		}
		a->val = c->val.y;
		c_drop ();
		push (a);
	}
	else
	{
		lerror ("IM", ERR_WRTYPE);
	}
}
