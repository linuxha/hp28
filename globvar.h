/****************************************************************

	Global variables

----------------------------------------------------------------

0.0	hjp	89-06-14

	initial version

0.1	hjp	89-07-08

	vars	added

0.2	hjp	89-07-14

	traceflag	added

0.3	hjp	89-08-28

	ip	added

0.4	hjp	89-09-03

	localvars added

0.5	hjp	89-11-23

	radix added

0.6	hjp	90-03-03

	constants added.

0.6	hjp	90-03-03

	main_loop added.

****************************************************************/


#ifndef I_globvar

	#define I_globvar

	#include <setjmp.h>
	#include "rpl.h"

	extern
	listobj * stack;

	extern
	varobj	* vars,
		* localvars;

	extern
	genobj	** ip;

#ifdef READLINE
extern char	*cmdline;  /*  command line  (malloc'd for realine) */
extern char	empty;	   /* is it empty	*/
extern char	*rdptr;	   /* rdptr points to the first character not yet read by getobj	*/
extern char	pbuffer[]; /* buffer for building programs */
#else
	extern
	char 	cmdline [],
		empty,
		* rdptr,

                pbuffer [];
#endif
	extern
	opobj ops [];

	extern
	const int noops;

	#define NOOPS	noops

	extern
	int	traceflag;

	extern
	int	radix;

	extern
	complexobj	complex_zero, complex_one, complex_i;

	extern
	realobj		real_zero, real_e, real_pi, real_max, real_min;

	extern
	jmp_buf		main_loop;

#endif
