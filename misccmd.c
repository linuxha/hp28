/****************************************************************

	Miscellaneous user commands

0.0	hjp	89-06-27

	initial version

0.1	hjp	89-07-14

	TRON, TROFF added

0.2	hjp	89-08-29

	TIME added

0.3	hjp	89-09-03

	TIME changed: uses ftime now instead of time

0.4	hjp	89-10-04

	-> added.

0.5	hjp	89-11-23

	BIN, OCT, DEC, HEX added.

0.6	hjp	90-02-27

	comment object added in local_var.

0.7	hjp	90-03-02

	} added.

0.8	hjp	90-03-03

	malloc replaced by mallocobj (at last!).

0.9	hjp	90-03-06

	sys/types.h added for UNIX-compatibility.

****************************************************************/


#include <math.h>
#include <process.h>
#include <string.h>
#include <sys/types.h>
#include <sys/timeb.h>

#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "intcmd.h"
#include "misccmd.h"
#include "debug.h"
#include "stackcmd.h"

/*
	exit from HP28-Emulator
*/

void	c_off (void)
{
	exit (0);
}


void c_pbegin (void)
{
	/*	dummy function -- does nothing	*/
}


void c_pend (void)
{
	/*	dummy function -- does nothing	*/
}


/*
	evaluate object in level 1
*/

void	c_eval (void)
{
	listobj * l;

	l = stack;

	if (l) {
		if (l->id == LIST) {
			stack = l->next;

			interprete (l->obj, 0);

			destroy ((genobj *)l, 0);
		} else {
			lerror ("eval", INT_STKNOLIST);
		}
	} else {
		lerror ("eval", ERR_STKEMPTY);
	}
}

void	c_tron (void)
{
	traceflag = 1;
}

void	c_troff (void)
{
	traceflag = 0;
}


void	c_time (void)
{
	struct timeb tb;

	realobj * c;

	ftime (&tb);
	if (!(c = mallocobj (REAL))) {
		lerror ("TIME", ERR_NOMEM);
		return;
	}
	c->val = tb.time + tb.millitm * 0.001;
	push (c);

}

/*
	->

	Syntax required:

		-> UNAME { UNAME } PROGRAM

	1. create local variable for each uname assigning values from the stack.
	2. execute program.
	3. delete the local variables created in step 1.

	Example:

		-> a b c << some program >>

		Stack:
			1
			2
			3
			4

		before execution of << some program >>, the stack will contain
		the single value 1,
		the local variable a will contain the value 2,
				   b will contain the value 3,
				   c will contain the value 4.

		This twisted arrangement (the variable found last by the interpreter
		gets the value in level one) makes programs more readable,
		but complicates the assignment to local variables.
*/

static
int	local_var (void)
{
	int	n_var;
	varobj	* v;
	nameobj	* n;

	if ((* ++ ip)->id == UNAME) {

		n = * ip;	/*	remember name	*/

		/*	create local variables following in the list first	*/

		n_var = local_var () + 1;

		/*	is there a value on the stack ?	*/

		if (! stack) {
			lerror ("->", ERR_2FEWARG);
			return (n_var - 1);
		}

		/* 	create local variable	*/

		if (! (v = mallocobj (VARIABLE))) {
			lerror ("->", ERR_NOMEM);
			return (n_var - 1);
		}

		/*	and move value from stack to new var.	*/

		v->id = VARIABLE;
		v->size = sizeof (varobj);
		v->link = 1;
		strcpy (v->name, n->name);
		v->val = stack->obj; stack->obj->link ++;
		v->next = localvars; localvars = v;
		c_drop ();
		return	n_var;
	} else if ((* ip)->id == COMMENT) {
		/*	skip it and try next	*/
		return local_var ();
	} else {
		return 0;
	}
}

void	c_local (void)
{
	int	n_var;		/*	number of local variables	*/

	n_var = local_var ();

	/*	now * ip should point to a program	*/

	if ((* ip)->id != PROGRAM) {
		lerror ("->", ERR_WRTYPE, id2str ((* ip)->id));
		return;
	}

	interprete ((* ip), 0);

	/*	remove all local variables created	*/

	while (-- n_var >= 0) {
		varobj * v = localvars->next;
		destroy (localvars, 0);
		localvars = v;
	}
}


/*
	BIN	set radix for binaries to 2.
*/

void c_bin (void)
{
	radix = 2;
}


/*
	OCT	set radix for binaries to 8.
*/

void c_oct (void)
{
	radix = 8;
}


/*
	DEC	set radix for binaries to 10.
*/

void c_dec (void)
{
	radix = 10;
}


/*
	HEX	set radix for binaries to 16.
*/

void c_hex (void)
{
	radix = 16;
}


void c_listend (void)
{
	/*	dummy function -- does nothing	*/
}
