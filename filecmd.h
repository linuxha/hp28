/****************************************************************

	File commands

0.0	hjp	89-08-15

	initial version

0.1	hjp	89-08-15

	SYSTEM added

0.2	hjp	89-12-02

	PRINT added.

****************************************************************/

#ifndef I_filecmd

	#define I_filecmd

	void	c_save		(void);
	void	c_load		(void);
	void	c_print		(void);
	void	c_system	(void);

#endif
