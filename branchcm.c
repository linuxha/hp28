/****************************************************************

Module:
	BranchCmd

Description:
	Commands for flow control


Modification history:

	0.0	hjp	89-07-14

		initial version

	0.1	hjp	89-08-29

		if-then-else-endif implemented.
		WARNING: HP28's END replaced by ENDIF to keep syntax out
			 of parser !!!
		start implemented.
		next implemented.

	0.2	hjp	89-09-04

		for implemented
		do-until-enddo implemented.
		WARNING: HP28's END replaced by ENDDO to keep syntax out
			 of parser !!!
		while-repeat-endwhile implemented.
		WARNING: HP28's END replaced by ENDWHILE to keep syntax out
			 of parser !!!

	0.3	hjp	89-10-05

		link count introduced in FOR ... NEXT | STEP.

	0.4	hjp	89-12-11

		minor bug fixing.

	0.5	hjp	90-03-03

		memmove () replaced by memcpy () for portability reasons.

	0.6	hjp	90-03-03

		malloc replaced by mallocobj (at last !)

****************************************************************/

#include <stddef.h>
#include <string.h>

#include "rpl.h"
#include "branchcm.h"
#include "debug.h"
#include "errors.h"
#include "globvar.h"
#include "intcmd.h"
#include "misccmd.h"
#include "stackcmd.h"

loopobj * loopstack = NULL;

void	c_ift (void)
{
	listobj	* a;

	if (stack && (a = stack->next)) {
		if (a->obj->id == REAL) {
			if (((realobj *)a->obj)->val) {
				c_swap ();
				c_drop ();
				c_eval ();
			} else {
				c_drop ();
				c_drop ();
			}
		} else {
			lerror ("IFT", ERR_WRTYPE, NULL);
		}
	} else {
		lerror ("IFT", ERR_2FEWARG, NULL);
	}
}


void	c_ifte (void)
{
	listobj	* a;

	if (stack && stack->next && (a = stack->next->next)) {
		if (a->obj->id == REAL) {
			if (((realobj *)a->obj)->val) {
				c_drop ();
				c_swap ();
				c_drop ();
				c_eval ();
			} else {
				c_swap ();
				c_drop ();
				c_swap ();
				c_drop ();
				c_eval ();
			}
		} else {
			lerror ("IFT", ERR_WRTYPE, NULL);
		}
	} else {
		lerror ("IFT", ERR_2FEWARG, NULL);
	}
}

void c_if (void)
{
	/*	dummy function	--  does nothing	*/
}

void c_then (void)
{
	genobj * a;
	int	level;

	if (! stack) {
		lerror ("THEN", ERR_2FEWARG, NULL);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (((realobj *)a)->val != 0.0) {

			/*	do nothing	*/

		} else {
			for (level = 1; level;) {
				ip ++;
				if ((* ip)->id == OP) {
					opobj * oop = (opobj *) * ip;
					if (((opobj *) * ip)->fptr == c_then) {
						level ++;
					} else if (((opobj *) * ip)->fptr == c_else) {
						if (level == 1) {
							level --;
						}
					} else if (((opobj *) * ip)->fptr == c_endif) {
						level --;
					}
				}
			}
		}
		c_drop ();
	} else {
		lerror ("THEN", ERR_WRTYPE, NULL);
	}
}


void c_else (void)
{
	int level;

	for (level = 1; level; ip ++) {
		if ((* ip)->id == OP) {
			opobj * oop = (opobj *) * ip;
			if (((opobj *) * ip)->fptr == c_then) {
				level ++;
			} else if (((opobj *) * ip)->fptr == c_endif) {
				level --;
			}
		}
	}
}


void c_endif (void)
{
	/*	dummy function -- does nothing	*/
}


void c_for (void)
{
	loopobj		* p;
	genobj		* a, * b;
	varobj		* v, * w;
	realobj		* c;

	if (! (stack && (b = stack->obj) && stack->next && (a = stack->next->obj))) {
		lerror ("FOR", ERR_2FEWARG, NULL);
		return;
	}
	if (! (b->id == REAL && a->id == REAL)) {
		lerror ("FOR", ERR_WRTYPE, NULL);
		return;
	}
	if ((w = (varobj *) (* ++ ip))->id != UNAME) {
		lerror ("FOR", ERR_SYNTAX, "Unquoted name expected");
		return;
	}

	if (! (p = mallocobj (FOR)) ||
	    ! (v = mallocobj (VARIABLE)) ||
	    ! (c = mallocobj (REAL))) {
		lerror ("FOR", ERR_NOMEM, NULL);
		return;
	}

	c->link = 1;
	c->val = ((realobj *)a)->val;

	v->link = 1;
	strcpy (v->name, w->name);
	v->val = c;
	v->next = localvars;
	localvars = v;

	p->link = 1;
	p->addr = ip;
	p->var = v;
	p->cnt = ((realobj *)b)->val;
	p->next = loopstack;
	loopstack = p;
	c_drop ();
	c_drop ();
}



void c_start (void)
{
	loopobj	* p;
	genobj	* a, * b;

	if (stack && (b = stack->obj) && stack->next && (a = stack->next->obj)) {
		if (b->id == REAL && a->id == REAL) {

			if (p = mallocobj (START)) {
				p->addr = ip;
				p->cnt = ((realobj *) b)->val - ((realobj *) a)->val;
				p->next = loopstack;
				loopstack = p;
				c_drop ();
				c_drop ();
			} else {
				lerror ("START", ERR_NOMEM, NULL);
			}
		} else {
			lerror ("START", ERR_WRTYPE, NULL);
		}
	} else {
		lerror ("START", ERR_2FEWARG, NULL);
	}
}


void c_next (void)
{
	loopobj * p;

	if (loopstack) {
		if (loopstack->id == START) {
			if (-- loopstack->cnt >= 0) {
				ip = loopstack->addr;
			} else {
				p = loopstack->next;

				destroy (loopstack, 1);
				loopstack = p;
			}
		} else if (loopstack->id == FOR) {
			realobj * p;

			if ((p = (realobj *) loopstack->var->val)->link > 1) {

				/* value in use by multiple objects -- create new object for it	*/

				if (! (p = mallocobj (REAL))) {
					lerror ("NEXT", ERR_NOMEM);
					return;
				}

				memcpy (p, loopstack->var->val, sizeof (realobj));
				p->link = 1;
				destroy (loopstack->var->val, 1);
				loopstack->var->val = p;

			} else if (p->link <= 0) {
				lerror ("NEXT", INT_BADLINK, p->link, id2str (p->id));
				return;
			}
			if ((p->val += 1) <= loopstack->cnt) {
				ip = loopstack->addr;
			} else {

				/* top of local variable stack is loop variable -- purge it	*/

				if (localvars == loopstack->var) {
					varobj * p = localvars;
					localvars = localvars->next;
					destroy (p, 1);
				} else {
					lerror ("NEXT", ERR_NXVAR, loopstack->var->name);
				}

				/* pop one item from loopstack	*/

				p = loopstack->next;

				destroy (loopstack, 1);
				loopstack = p;
			}
		} else {
			lerror ("NEXT", ERR_LOOP, NULL);
		}
	} else {
		lerror ("NEXT", ERR_LOOP, NULL);
	}
}


void c_step (void)
{
	realobj * a;

	if (! loopstack) {
		lerror ("STEP", ERR_LOOP, NULL);
		return;
	}

	if (! stack) {
		lerror ("STEP", ERR_2FEWARG, NULL);
		return;
	}
	if ((a = stack->obj)->id != REAL) {
		lerror ("STEP", ERR_WRTYPE, NULL);
		return;
	}
	if (loopstack->id == START) {

		if ((loopstack->cnt -= a->val) > 0.0) {
			ip = loopstack->addr;
		} else {
			loopobj * p = loopstack->next;

			destroy (loopstack, 1);
			loopstack = p;
		}
		c_drop ();
	} else if (loopstack->id == FOR) {
		realobj * p;

		if ((p = (realobj *) loopstack->var->val)->link > 1) {

			/* value in use by multiple objects -- create new object for it	*/

			if (! (p = mallocobj (REAL))) {
				lerror ("STEP", ERR_NOMEM);
				return;
			}

			memcpy (p, loopstack->var->val, sizeof (realobj));
			p->link = 1;
			destroy (loopstack->var->val, 1);
			loopstack->var->val = p;

		} else if (p->link <= 0) {
			lerror ("STEP", INT_BADLINK, p->link, id2str (p->id));
			return;
		}
		if (a->val >= 0
		    ? (p->val += a->val) <= loopstack->cnt
		    : (p->val += a->val) >= loopstack->cnt) {
			ip = loopstack->addr;
		} else {

			/* top of local variable stack is loop variable -- purge it	*/

			if (localvars == loopstack->var) {
				varobj * p = localvars;
				localvars = localvars->next;
				destroy (p, 1);
			} else {
				lerror ("NEXT", ERR_NXVAR, loopstack->var->name);
			}

			/* pop one item from loopstack	*/

			p = loopstack->next;

			destroy (loopstack, 1);
			loopstack = p;
		}
		c_drop ();
	} else {
		lerror ("NEXT", ERR_LOOP, NULL);
	}
}


void c_do (void)
{
	loopobj * p;

	if (p = mallocobj (DO)) {
		p->addr = ip;
		p->next = loopstack;
		loopstack = p;
	} else {
		lerror ("DO", ERR_NOMEM, NULL);
	}
}


void c_until (void)
{
	/*	dummy function -- does nothing	*/
}

void c_enddo (void)
{
	loopobj	* p;
	genobj	* a;

	if (! loopstack || loopstack->id != DO) {
		lerror ("ENDDO", ERR_LOOP, NULL);
		return;
	}
	if (! stack) {
		lerror ("ENDDO", ERR_2FEWARG, NULL);
		return;
	}
	if ((a = stack->obj)->id != REAL) {
		lerror ("ENDDO", ERR_WRTYPE, NULL);
		return;
	}
	if (((realobj *)a)->val) {
		p = loopstack->next;

		destroy (loopstack, 1);
		loopstack = p;
	} else {
		ip = loopstack->addr;
	}
	c_drop ();
}


void c_while (void)
{
	loopobj * p;

	if (p = mallocobj (WHILE)) {
		p->addr = ip;
		p->next = loopstack;
		loopstack = p;
	} else {
		lerror ("WHILE", ERR_NOMEM, NULL);
	}
}

void c_repeat (void)
{
	loopobj	* p;
	genobj	* a;
	int	level;

	if (! stack) {
		lerror ("REPEAT", ERR_2FEWARG, NULL);
		return;
	}
	if (! loopstack || loopstack->id != WHILE) {
		lerror ("REPEAT", ERR_LOOP, NULL);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (((realobj *)a)->val != 0.0) {

			/*	do nothing	*/

		} else {
			for (level = 1; level;) {
				ip ++;
				if ((* ip)->id == OP) {
					opobj * oop = (opobj *) * ip;
					if (oop->fptr == c_while) {
						level ++;
					} else if (oop->fptr == c_endwhile) {
						level --;
					}
				}
			}
			p = loopstack->next;

			destroy (loopstack, 1);
			loopstack = p;
		}
		c_drop ();
	} else {
		lerror ("REPEAT", ERR_WRTYPE, NULL);
	}
}


void c_endwhile (void)
{
	if (! loopstack || loopstack->id != WHILE) {
		lerror ("ENDWHILE", ERR_LOOP, NULL);
		return;
	}
	ip = loopstack->addr;
}

