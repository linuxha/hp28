/****************************************************************

Module:
	Debug

Description:
	Contains variables of all types to enable casts in the debugger.


Modification history:

	0.0	hjp	89-07-08

		initial version

	0.2	hjp	89-08-28

		opobjs added

****************************************************************/

#ifndef I_debug

	#include "rpl.h"

	extern nameobj	 no;
	extern nameobj	 * nop;
	extern stringobj so;
	extern stringobj * sop;
	extern opobj	 oo;
	extern opobj	 * oop;

	void * debugmalloc(unsigned);
	void   debugfree(void *);
	void * debugrealloc(void *, unsigned);

	void	memmap(void);
	void	clearmem(uint);

#endif
