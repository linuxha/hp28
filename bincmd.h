/****************************************************************

	Commands related to binary objects.

0.0	hjp	90-03-03

	initial version.

****************************************************************/

#ifndef I_bincmd

	#define I_bincmd

	void	c_b_r	(void);
	void	c_r_b	(void);

#endif
