/****************************************************************

Module:
	LogCmd

Description:
	Commands related to logarithms (The LOGS menu on HP28C)

Modification history:

	0.0	hjp	89-12-03

		initial version
		LN and EXP extracted from ArithCmd.

****************************************************************/

#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "intcmd.h"
#include "logcmd.h"
#include "stackcmd.h"

/*
	ln	--	compute natural logarithm of object in level 1

	x	->	ln(x)

	REAL	->	REAL
	REAL	->	COMPLEX
	COMPLEX ->	COMPLEX
*/

void c_ln (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("ln", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (((realobj *)a)->val >= 0.0) {
			if (!(c = mallocobj (REAL)))
			{
				lerror ("LN", ERR_NOMEM);
				return;
			}
			c->id = REAL;
			c->link = 0;
			c->size = sizeof (realobj);
			c->val = log (((realobj *) a)->val);
			c_drop ();
			push (c);
		} else {
			if (! (b = mallocobj (COMPLEX)))
			{
				lerror ("LN", ERR_NOMEM);
				return;
			}
			b->val.x = log (-((realobj *) a)->val);
			b->val.y = M_PI;
			c_drop ();
			push (b);
		}
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		{
			lerror ("LN", ERR_NOMEM);
			return;
		}
		b->val.x = log (sqrt (x * x + y * y));
		b->val.y = x || y ? atan2 (y, x) : 0.0;
		c_drop ();
		push (b);
	} else {
		lerror ("inv", ERR_WRTYPE);
	}
}

/*
	EXP	compute e to the power of object in level 1

	x	->	e ^ x

	real	->	real
	complex ->	complex
*/

void c_exp (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("EXP", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("EXP", ERR_NOMEM);
			return;
		}
		c->val = exp (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		{
			lerror ("EXP", ERR_NOMEM);
			return;
		}
		b->val.x = exp (x) * cos (y);
		b->val.y = exp (x) * sin (y);
		c_drop ();
		push (b);
	} else {
		lerror ("EXP", ERR_WRTYPE);
	}
}


void
c_alog(void) {
  /*
pow(base, x) == exp(x * log(base)) :


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <time.h>


int
main(void) {
  double x, y;
  int i;

  double M_log10 = log(10);

  srand(time(0));

  printf("exp(x) is the antilog(x, base=%.*g)\n\n", DBL_DIG, exp(1));

  for (i = 0; i < 10; i++) {
    x = rand() / (1. + RAND_MAX);
    y = exp(x),

    printf("exp(%g) = %g, log(%g) = %g\n", x, y, y, log(y));

    y = pow(10, x);

    printf("pow(10, %g) = %g, log10(%g) = %g\n", x, y, y, log10(y));

    y = exp(x * M_log10);

    printf("exp(%g * log(10)) = %g, log10(%g) = %g\n\n", x, y, y, log10(y));
  }

  return 0;
}


This is the result of one run:

exp(x) is the antilog(x, base=2.71828182845905)

exp(0.54030) = 1.71654, log(1.71654) = 0.540308
pow(10, 0.54030) = 3.46983, log10(3.46983) = 0.540308
exp(0.540308 * log(10)) = 3.46983, log10(3.46983) = 0.540308

exp(0.655142) = 1.92542, log(1.92542) = 0.655142
pow(10, 0.655142) = 4.52004, log10(4.52004) = 0.655142
exp(0.655142 * log(10)) = 4.52004, log10(4.52004) = 0.655142

exp(0.430014) = 1.53728, log(1.5372 = 0.430014
pow(10, 0.430014) = 2.69162, log10(2.69162) = 0.430014
exp(0.430014 * log(10)) = 2.69162, log10(2.69162) = 0.430014

exp(0.656084) = 1.92723, log(1.92723) = 0.656084
pow(10, 0.656084) = 4.52985, log10(4.52985) = 0.656084
exp(0.656084 * log(10)) = 4.52985, log10(4.52985) = 0.656084

exp(0.567552) = 1.76394, log(1.76394) = 0.567552
pow(10, 0.567552) = 3.69447, log10(3.69447) = 0.567552
exp(0.567552 * log(10)) = 3.69447, log10(3.69447) = 0.567552

exp(0.346806) = 1.41454, log(1.41454) = 0.346806
pow(10, 0.346806) = 2.22232, log10(2.22232) = 0.346806
exp(0.346806 * log(10)) = 2.22232, log10(2.22232) = 0.346806

exp(0.418147) = 1.51914, log(1.51914) = 0.418147
pow(10, 0.418147) = 2.61907, log10(2.61907) = 0.418147
exp(0.418147 * log(10)) = 2.61907, log10(2.61907) = 0.418147

exp(0.859071) = 2.36097, log(2.36097) = 0.859071
pow(10, 0.859071) = 7.22888, log10(7.2288 = 0.859071
exp(0.859071 * log(10)) = 7.22888, log10(7.2288 = 0.859071

exp(0.824404) = 2.28052, log(2.28052) = 0.824404
pow(10, 0.824404) = 6.67427, log10(6.67427) = 0.824404
exp(0.824404 * log(10)) = 6.67427, log10(6.67427) = 0.824404

exp(0.225011) = 1.25234, log(1.25234) = 0.225011
pow(10, 0.225011) = 1.67885, log10(1.67885) = 0.225011
exp(0.225011 * log(10)) = 1.67885, log10(1.67885) = 0.225011


>can you tell me how to use this function to find the antilog of
>0.something to base 10, and also how to make it work for different bases[/color]

(Using non-C notation in this portion)

Log{N}[x] -> ln[x] / ln[N]

so

ln[x] -> Log{N}[x] * ln[N]

so

exp[ln[x]] -> exp[ Log{N}[x] * ln[N] ]

hence

x -> exp[ Log{N}[x] * ln[N] ]

Hence, to find the antilog of something base 10, Y, take
(using C notation)

exp(Y * log(10.))


For example, log10(1000.) = 3., ln(10.) ~= 2.3025851, and
exp( 3. * 2.3025851 ) = exp( 6.9077553 ) ~= 1000.0


> exp(Y * log(10.))

Or simply:

pow(10., Y)
  */
}

