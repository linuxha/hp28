/****
	hjp.h

	Functions, Macros and Types often used

****/
/****
	Modification history:

	1.0	88-07-06	Not really the first release which was lost some time ago.	hjp
	1.1	89-06-13	ushort added;
				version changed to Version
				eprintf added							hjp
	1.2	90-03-06	#ifndef _TYPES_ added for UNIX-compatibility.

****/

#ifndef HJP

#define HJP

#define schar	static char

typedef unsigned char	uchar;		/* 8 bit	*/

#ifndef _TYPES_
typedef unsigned short	ushort;		/* 16 bit	*/
typedef unsigned int	uint;		/* 16 bit	*/
#endif
typedef unsigned long	ulong;		/* 32 bit	*/

#define Version(a) schar VER[] = a;

void prof_start (char * filename);

#endif
