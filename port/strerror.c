/*
	strerror.c

	replacement for Turbo-Cs strerror function
	(nonexistent on UNIX systems)
*/

extern int sys_nerr;
extern char * sys_errlist [];

char * strerror (int errno)
{
	char unknown [] = "Unknown error";

	if (errno < 0 || errno >= sys_nerr) {
		return unknown;
	} else {
		return sys_errlist [errno];
	}
}
