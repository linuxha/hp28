/*
	itoa.c

	replacement for itoa and ultoa functions
	(nonexistent on UNIX)
*/

static char digits [] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

char * itoa (int value, char * string, int radix)
{
	char	buffer [33];
	char * s = string, * b = buffer;

	if (radix < 2 || radix > 36) return 0;
	if (value < 0) {
		* s ++ = '-';
		value = - value;
	}

	while (value) {
		* b ++ = digits [value % radix];
		value /= radix;
	}
	if (b == buffer) {	/* value was zero	*/
		*s ++ = '0';
	} else {
		while (--b >= buffer) {
			* s ++ = * b;
		}
	}
	*s = 0;

	return string;
}

char * ultoa (unsigned long value, char * string, int radix)
{
	char	buffer [33];
	char * s = string, * b = buffer;

	if (radix < 2 || radix > 36) return 0;

	while (value) {
		* b ++ = digits [value % radix];
		value /= radix;
	}
	if (b == buffer) {	/* value was zero	*/
		*s ++ = '0';
	} else {
		while (--b >= buffer) {
			* s ++ = * b;
		}
	}
	*s = 0;

	return string;
}

