
/*
	port.h

	this file contains types and constants which may have to be
	changed for other compilers or machines.

*/

#ifndef I_port
#define I_port

#if defined (HUGE_VAL) && defined (__TURBOC__)
#undef HUGE_VAL
#define HUGE_VAL	1.7976931348623157e+308
#endif

#ifndef HUGE_VAL
#define HUGE_VAL	1.7976931348623157e+308
#endif

#ifndef TINY_VAL
#define TINY_VAL	2.2250738585072014e-308
#endif

#ifdef unix

struct complex {double x, y;};
extern double mycabs (struct complex z);

#endif

#ifdef mips
#define host_mips
#endif

#endif
