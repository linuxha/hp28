/*
	strtoul.c 

	replacement for Turbo-Cs strtoul function.
	(missing on UNIX)

*/

#include <ctype.h>

unsigned long strtoul (char * s, char ** endptr, int radix)
{
	unsigned long	ul = 0;
	int		limit = radix > 9 ? radix - 10 + 'A' : radix + '0';
	int		c;

	while ((c = toupper (*s)) && isalnum (c) && c < limit) {
		ul = ul * radix + (c > '9' ? c + 10 - 'A' : c - '0');
		s ++;
	}
	* endptr = s;
	return ul;
}
