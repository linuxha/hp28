/****************************************************************

	Parser.h

0.0	hjp	89-06-14

	initial version

****************************************************************/

#ifdef READLINE
extern char	*cmdline;  /*  command line  (malloc'd for realine) */
extern char	empty;	   /* is it empty	*/
extern char	*rdptr;	   /* rdptr points to the first character not yet read by getobj	*/
extern char	pbuffer[]; /* buffer for building programs */
#else
extern
char	cmdline [],				/* The command line     */
	empty,					/* is it empty	*/
	* rdptr,				/* rdptr points to the first character not yet read by getobj	*/

	pbuffer []; 				/* buffer for building programs */
#endif

void	findwhite (void);
void	skipwhite (void);
genobj	* getobj (void);
void	edit	(genobj * obj);
void	* readvalue (void);
