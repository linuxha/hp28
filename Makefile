#	rpl Makefile
#
#	This Makefile works with ULTRIX v2.1 and GCC v.1.36.
#	You may have to make changes for other systems or
#	compilers.

#	@(#)	1.0	90-03-07	hjp


#	port contains headerfiles I wrote as replacements
#	for some of Turbo-C's headerfiles during porting 
#	the program to ULTRIX.

#	define -DTRACE	for some tracing messages.
#	I added them because gcc and dbx won't work together.

#	define -DSTDARGBUG if stdarg.h doesn't work right
#	with your compiler, too.

#CFLAGS = -Iport -DSTDARGBUG
CFLAGS1  =	-Iport -Dunix -DREADLINE
#CFLAGS1  =	-Iport -Dunix
CFLAGS   =       -g -O3 -Wall -std=gnu99 $(CFLAGS1) 
#FLAGS   =       -g -O0 $(CFLAGS1)
LFLAGS   =       
#CC       =       gcc $(CFLAGS)
CC       =       gcc

#	object files for rpl.

OBJS = 	arithcmd.o	\
	bincmd.o	\
	branchcm.o	\
	cmplxcmd.o	\
	errors.o	\
	filecmd.o	\
	globvar.o	\
	rpl.o		\
	intcmd.o	\
	logcmd.o	\
	matherr.o	\
	misccmd.o	\
	parser.o	\
	realcmd.o	\
	relcmd.o	\
	stackcmd.o	\
	storecmd.o	\
	trigcmd.o	\
	version.o

#	additional object files needed with gcc.
#	They may be in your C library or you may miss others ...

POBJS = cabs.o		\
	itoa.o		\

#	strerror.o	\
#	strtoul.o

FILES = arithcmd.c \
	arithcmd.h \
	bincmd.c \
	bincmd.h \
	branchcm.c \
	branchcm.h \
	cmplxcmd.c \
	cmplxcmd.h \
	debug.c \
	debug.h \
	errors.c \
	errors.h \
	filecmd.c \
	filecmd.h \
	globvar.c \
	globvar.h \
	intcmd.c \
	intcmd.h \
	logcmd.c \
	logcmd.h \
	matherr.c \
	mem.h \
	misccmd.c \
	misccmd.h \
	parser.c \
	parser.h \
	realcmd.c \
	realcmd.h \
	relcmd.c \
	relcmd.h \
	rpl.c \
	rpl.h \
	stackcmd.c \
	stackcmd.h \
	storecmd.c \
	storecmd.h \
	trigcmd.c \
	trigcmd.h

#	Libraries:
#		m is the math library
#		malloc contains faster memory allocation routines.

#LIBS = -lm -lmalloc
LIBS = -lm -lreadline
#LIBS = -lm

all:	rpl

rpl:	$(OBJS) $(POBJS)
	$(CC) -o rpl $(OBJS) $(POBJS) $(LIBS)

arithcmd.o:	arithcmd.c	arithcmd.h	debug.h errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h
bincmd.o:	bincmd.c	branchcm.h	debug.h errors.h	rpl.h	intcmd.h	misccmd.h	stackcmd.h
branchcm.o:	branchcm.c	branchcm.h	debug.h errors.h	rpl.h	intcmd.h	misccmd.h	stackcmd.h
cmplxcmd.o:	cmplxcmd.c	cmplxcmd.h	errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h
debug.o:	debug.c 	debug.h	rpl.h	errors.h
errors.o:	errors.c	errors.h	debug.h
filecmd.o:	filecmd.c	errors.h	filecmd.h	globvar.h	rpl.h	intcmd.h
globvar.o:	globvar.c	arithcmd.h	branchcm.h	cmplxcmd.h	debug.h filecmd.h	globvar.h	rpl.h	logcmd.h	misccmd.h	relcmd.h	stackcmd.h	storecmd.h	trigcmd.h
rpl.o:	rpl.c		debug.h	globvar.h	rpl.h	intcmd.h	parser.h
intcmd.o:	intcmd.c	debug.h	errors.h	rpl.h	globvar.h	intcmd.h	misccmd.h
logcmd.o:	logcmd.c	errors.h	globvar.h	rpl.h	intcmd.h	logcmd.h	stackcmd.h
matherr.o:	matherr.c
misccmd.o:	misccmd.c	debug.h	errors.h	globvar.h	rpl.h	intcmd.h	misccmd.h	stackcmd.h
parser.o:	parser.c	debug.h	errors.h	globvar.h	rpl.h	misccmd.h	parser.h
realcmd.o:	realcmd.c	arithcmd.h	errors.h	globvar.h	intcmd.h	realcmd.h	rpl.h	stackcmd.h
relcmd.o:	relcmd.c	relcmd.h	debug.h	errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h
stackcmd.o:	stackcmd.c	debug.h	errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h
storecmd.o:	storecmd.c	debug.h	errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h	storecmd.h
trigcmd.o:	trigcmd.c	debug.h	errors.h	globvar.h	rpl.h	intcmd.h	stackcmd.h	trigcmd.h
version.o:	${FILES}

cabs.o:	port/cabs.c
	$(CC) -c -o cabs.o port/cabs.c

itoa.o:	port/itoa.c
	$(CC) -c -o itoa.o port/itoa.c

strerror.o:	port/strerror.c
	$(CC) -c -o strerror.o port/strerror.c

strtoul.o:	port/strtoul.c
	$(CC) -c -o strtoul.o port/strtoul.c

#
#	clean: remove all files created during making
#

clean:
	rm -rf $(OBJS) $(POBJS) core *~ rpl

