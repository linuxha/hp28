/****************************************************************

	Stack commands

0.0	hjp	89-06-14

	initial version: DROP, SWAP, CLEAR

0.1	hjp	89-07-14

	DUP added

****************************************************************/

#ifndef I_stackcmd

	#define I_stackcmd

	void	c_drop	(void);
	void	c_swap	(void);
	void	c_clear	(void);
	void	c_dup	(void);

#endif
