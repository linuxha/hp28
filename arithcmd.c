/****************************************************************

Module:
	ArithCmd

Description:
	Commands for manipulating arithmetic objects


Modification history:

	0.0	hjp	89-06-26

		initial version

	0.1	hjp	89-07-14

		size field added

	0.2	hjp	89-09-04

		link field added.

	0.3	hjp	89-10-01

		EXP and LN added.

	0.4	hjp	89-11-08

		PI and e added.
		SIN, ASIN, COS, ACOS, TAN, ATAN added.

	0.5	hjp	89-11-11

		Bugs in SQ, ACOS, ATAN fixed.

	0.6	hjp	89-11-15

		C->R, R->C, RE, IM added.

	0.7	hjp	89-11-23

		support for binary objects added.
		(in functions:	ADD)

	0.8	hjp	89-12-02

		support for binary objects added.
		(in functions:	SUB, MUL, DIV)

	0.9	hjp	89-12-03

		LN and EXP moved to LogCmd
		SIN, TAN, COS, ASIN, ATAN, ACOS moved to TrigCmd
		R->C, C->R, RE, IM moved to CmplxCmd.
		malloc replaced by mallocobj in remaining functions.

	0.10	hjp	89-12-11

		bug fixing in ADD, SUB, MUL, DIV, ^: # of params wrong.

	0.11	hjp	90-03-03

		constants pi, e, i moved to globvar.c

****************************************************************/

#include "arithcmd.h"
#include "globvar.h"
#include "errors.h"
#include "rpl.h"
#include "intcmd.h"
#include "stackcmd.h"

#include "debug.h"


/*
**
*/

#if(0)
double
mylcabs(complexobj *j) {
  double d;
  double complex z;
  
  z.x = j->val.x;
  z.y = j->val.y;
		
  d = cabs(z);
  return(d);
}
#endif

/*
	add the 2 topmost elements
*/

void
c_add (void) {
	genobj * a, * b, * c;

	if (! stack || ! stack->next)
	{
		lerror ("+", ERR_2FEWARG);
		return;
	}

	a = stack->obj; b = stack->next->obj;

	switch (t22int(a->id, b->id))
	{
	case t22int(REAL, REAL):
		if (!(c = mallocobj (REAL)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = ((realobj *) a)->val + ((realobj *) b)->val;
		break;
	case t22int(COMPLEX, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x + ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y + ((complexobj *) b)->val.y;
		break;
	case t22int(COMPLEX, REAL):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x + ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y;
		break;
	case t22int(REAL, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x + ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y =			       ((complexobj *) b)->val.y;
		break;
	case t22int(BINARY, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((binaryobj *) a)->val + ((binaryobj *) b)->val;
		break;
	case t22int(REAL, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((realobj *) a)->val + ((binaryobj *) b)->val;
		break;
	case t22int(BINARY, REAL):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("+", ERR_NOMEM);
			return;
		}
		((binaryobj  *)c)->val = ((binaryobj *) a)->val + ((realobj *) b)->val;
		break;
	default:
		lerror ("+", ERR_WRTYPE);
		return;
	}
	c_drop ();
	c_drop ();
	push (c);
}


/*
	subtract element in level 1 from element in level 2
*/

void c_sub (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("-", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	switch (t22int(a->id, b->id))
	{
	case t22int(REAL, REAL):
		if (!(c = mallocobj (REAL)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = ((realobj *) a)->val - ((realobj *) b)->val;
		break;
	case t22int(COMPLEX, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x - ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y - ((complexobj *) b)->val.y;
		break;
	case t22int(COMPLEX, REAL):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x - ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y;
		break;
	case t22int(REAL, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x - ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y =			       ((complexobj *) b)->val.y;
		break;
	case t22int(BINARY, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((binaryobj *) a)->val - ((binaryobj *) b)->val;
		break;
	case t22int(REAL, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((realobj *) a)->val - ((binaryobj *) b)->val;
		break;
	case t22int(BINARY, REAL):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("-", ERR_NOMEM);
			return;
		}
		((binaryobj  *)c)->val = ((binaryobj *) a)->val - ((realobj *) b)->val;
		break;
	default:
		lerror ("-", ERR_WRTYPE);
		return;
	}
	c_drop ();
	c_drop ();
	push (c);
}


/*
	multiply 2 topmost elements
*/

void c_mul (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("*", ERR_2FEWARG);
		return;
	}

	a = stack->obj; b = stack->next->obj;

	switch (t22int(a->id, b->id))
	{
	case t22int(REAL, REAL):
		if (!(c = mallocobj (REAL)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = ((realobj *) a)->val * ((realobj *) b)->val;
		break;
	case t22int(COMPLEX, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		{
			double	x1, y1, x2, y2;

			x1 = ((complexobj *) a)->val.x;
			x2 = ((complexobj *) b)->val.x;
			y1 = ((complexobj *) a)->val.y;
			y2 = ((complexobj *) b)->val.y;
			((complexobj *)c)->val.x = x1 * x2 - y1 * y2;
			((complexobj *)c)->val.y = x1 * y2 + x2 * y1;
		}
		break;
	case t22int(COMPLEX, REAL):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x * ((realobj *) b)->val;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y * ((realobj *) b)->val;
		break;
	case t22int(REAL, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((realobj *) a)->val * ((complexobj *) b)->val.x;
		((complexobj *)c)->val.y = ((realobj *) a)->val * ((complexobj *) b)->val.y;
		break;
	case t22int(BINARY, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((binaryobj *) a)->val * ((binaryobj *) b)->val;
		break;
	case t22int(REAL, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((realobj *) a)->val * ((binaryobj *) b)->val;
		break;
	case t22int(BINARY, REAL):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("*", ERR_NOMEM);
			return;
		}
		((binaryobj  *)c)->val = ((binaryobj *) a)->val * ((realobj *) b)->val;
		break;
	default:
		lerror ("*", ERR_WRTYPE);
		return;
	}
	c_drop ();
	c_drop ();
	push (c);
}


/*
	divide element in level 2 by element in level 1
*/

void c_div (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("/", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	switch (t22int(a->id, b->id))
	{
	case t22int(REAL, REAL):
		if (!(c = mallocobj (REAL)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = ((realobj *) a)->val / ((realobj *) b)->val;
		break;
	case t22int(COMPLEX, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		{
			double	x1, y1, x2, y2;

			x1 = ((complexobj *) a)->val.x;
			x2 = ((complexobj *) b)->val.x;
			y1 = ((complexobj *) a)->val.y;
			y2 = ((complexobj *) b)->val.y;
			((complexobj *)c)->val.x = (x1 * x2 + y1 * y2) / (x2 * x2 + y2 * y2);
			((complexobj *)c)->val.y = (x2 * y1 - x1 * y2) / (x2 * x2 + y2 * y2);
		}
		break;
	case t22int(COMPLEX, REAL):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = ((complexobj *) a)->val.x / ((realobj *) b)->val;
		((complexobj *)c)->val.y = ((complexobj *) a)->val.y / ((realobj *) b)->val;
		break;
	case t22int(REAL, COMPLEX):
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		{
			double	x, y, u;

			u = ((realobj *)a)->val;
			x = ((complexobj *) b)->val.x;
			y = ((complexobj *) b)->val.y;

			((complexobj *)c)->val.x = u * x / (x * x + y * y);
			((complexobj *)c)->val.y = - u * y / (x * x + y * y);
		}
		break;
	case t22int(BINARY, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((binaryobj *) a)->val / ((binaryobj *) b)->val;
		break;
	case t22int(REAL, BINARY):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		((binaryobj *)c)->val = ((realobj *) a)->val / ((binaryobj *) b)->val;
		break;
	case t22int(BINARY, REAL):
		if (!(c = mallocobj (BINARY)))
		{
			lerror ("/", ERR_NOMEM);
			return;
		}
		((binaryobj  *)c)->val = ((binaryobj *) a)->val / ((realobj *) b)->val;
		break;
	default:
		lerror ("/", ERR_WRTYPE);
	}
	c_drop ();
	c_drop ();
	push (c);
}


/*
	negate element in level 1
*/

void c_neg (void)
{
	genobj * a, * c;

	if (! stack) {
		lerror ("NEG", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("NEG", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = - ((realobj *) a)->val;
		c_drop ();
		push (c);
	} else if ((a = stack->obj)->id == COMPLEX) {
		if (!(c = mallocobj (COMPLEX)))
		{
			lerror ("NEG", ERR_NOMEM);
			return;
		}
		((complexobj *)c)->val.x = - ((complexobj *) a)->val.x;
		((complexobj *)c)->val.y = - ((complexobj *) a)->val.y;
		c_drop ();
		push (c);
	} else {
		lerror ("neg", ERR_WRTYPE);
	}
}


/*
	square element in level 1
*/

void c_sq (void)
{
	genobj		* a;
	complexobj	* b;
	realobj 	* c;

	if (! stack) {
		lerror ("SQ", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
	  if (!(c = (realobj *) mallocobj (REAL)))
		{
			lerror ("SQ", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = (((realobj *) a)->val) * (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if ((a = stack->obj)->id == COMPLEX) {
		double	x, y;

		x = ((complexobj *) a)->val.x;
		y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		b->val.x = x * x - y * y;
		b->val.y = 2 * x * y;

		c_drop ();
		push (b);

	} else {
		lerror ("SQ", ERR_WRTYPE);
	}
}


/*
	take square root of element in level 1
*/

void c_sqrt (void)
{
	genobj		* a;
	complexobj	* b;
	realobj 	* c;

	if (! stack) {
		lerror ("SQRT", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (((realobj *)a)->val >= 0.0) {
			if (!(c = mallocobj (REAL)))
			{
				lerror ("SQRT", ERR_NOMEM);
				return;
			}
			((realobj *)c)->val = sqrt(((realobj *) a)->val);
			c_drop ();
			push (c);
		} else {
			if (!(b = mallocobj (COMPLEX)))
			{
				lerror ("SQRT", ERR_NOMEM);
				return;
			}
			b->val.x = 0.0;
			b->val.y = sqrt(-((realobj *) a)->val);

			c_drop ();
			push (b);
		}
	} else if (a->id == COMPLEX) {
		double r, theta;

		r = mycabs (((complexobj *) a)->val); // Original code

		theta = ( ((complexobj *) a)->val.y || ((complexobj *) a)->val.x )
		           ? atan2 (((complexobj *) a)->val.y, ((complexobj *) a)->val.x)
		           : 0.0;

		if (!(b = mallocobj (COMPLEX)))
		{
			lerror ("SQRT", ERR_NOMEM);
			return;
		}
		((complexobj *)b)->val.x = sqrt (r) * cos (theta / 2);
		((complexobj *)b)->val.y = sqrt (r) * sin (theta / 2);

		c_drop ();
		push (b);

	} else {
		lerror ("sqrt", ERR_WRTYPE);
	}

}


/*
	take inverse of element in level 1
*/

void c_inv (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("INV", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL && ((realobj *)a)->val != 0.0) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("INV", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = 1/(((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (!(b = mallocobj (COMPLEX)))
		{
			lerror ("INV", ERR_NOMEM);
			return;
		}
		((complexobj *) b)->val.x = x / (x* x + y * y);
		((complexobj *) b)->val.y = - y / (x * x + y * y);
		c_drop ();
		push (b);
	} else {
		lerror ("INV", ERR_WRTYPE);
	}
}


/*
	z1	z2	->	z1^z2
*/

void c_pow (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("^", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL && (b = stack->next->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("^", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = pow (((realobj *) b)->val, ((realobj *) a)->val);
		c_drop ();
		c_drop ();
		push (c);
	} else {
		lerror ("^", ERR_WRTYPE);
	}
}


/*
	PI	push the numeric value of PI onto the stack.

	->	real
*/

void c_pi (void)
{
	push (&real_pi);
}


/*
	e	push the numeric value of e onto the stack.

	->	real
*/

void c_e (void)
{
	push (&real_e);
}


/*
	i	push the numeric value of i (sqrt (-1)) onto the stack.

	->	(0, 1)

	->	complex
*/

void c_i (void)
{
	push (&complex_i);
}
