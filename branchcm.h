/****************************************************************

Module:
	BranchCmd

Description:
	Commands for flow control


Modification history:

	0.0	hjp	89-07-14

		initial version

	0.1	hjp	89-08-28

		if-then-else-endif and loops added.
		WARNING: Syntax differs from HP28
			 END replaced by ENDIF, ENDDO, ENDWHILE !!!

****************************************************************/

#ifndef I_branchcm

	#define I_branchcm

	void	c_ift	(void);
	void	c_ifte	(void);
	void	c_if	(void);
	void	c_then	(void);
	void	c_else	(void);
	void	c_endif	(void);
	void	c_start	(void);
	void	c_for	(void);
	void	c_next	(void);
	void	c_step	(void);
	void	c_while	(void);
	void	c_repeat(void);
	void	c_endwhile	(void);
	void	c_do	(void);
	void	c_until	(void);
	void	c_enddo	(void);

	extern
	loopobj	* loopstack;

#endif
