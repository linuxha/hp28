/****************************************************************

Module:
	GlobVar

Description:
	global variables


Modification history:

	0.0	hjp	89-06-26

		initial version

	0.1	hjp	89-07-08

		vars added

	0.2	hjp	89-08-28

		ip added

	0.3	hjp	89-09-03

		localvars added

	0.4	hjp	89-11-23

		radix added.

	0.5	hjp	90-03-03

		bincmd added.
		constants added.

	0.6	hjp	90-03-04

		realcmd added.

	0.7	hjp	90-03-06

		opobj "MemMap" excluded for non Turbo-C environment.

	0.8	hjp	90-03-07

		main_loop added.

****************************************************************/

#include <setjmp.h>
#include <stddef.h>

#include "rpl.h"
#include "arithcmd.h"
#include "bincmd.h"
#include "branchcm.h"
#include "cmplxcmd.h"
#include "debug.h"
#include "filecmd.h"
#include "logcmd.h"
#include "misccmd.h"
#include "realcmd.h"
#include "relcmd.h"
#include "stackcmd.h"
#include "storecmd.h"
#include "trigcmd.h"

extern void c_dump(void); // Why do I need to do this for only this one ?
extern void c_version(void);

listobj * stack = NULL;

varobj	* vars = NULL,		/* user variables	*/
	* localvars = NULL;	/* local variables: created by FOR and -> operators	*/

int	radix = 16;		/* radix for i/o of binaries (2, 8, 10 or 16)	*/

genobj	** ip;

opobj ops [] = {
	OP,	0,	sizeof (opobj), c_add,		"+",
	OP,	0,	sizeof (opobj), c_sub,		"-",
	OP,	0,	sizeof (opobj), c_mul,		"*",
	OP,	0,	sizeof (opobj), c_div,		"/",
	OP,	0,	sizeof (opobj), c_drop, 	"DROP",
	OP,	0,	sizeof (opobj), c_off,		"OFF",
	OP,	0,	sizeof (opobj), c_neg,		"NEG",
	OP,	0,	sizeof (opobj), c_swap, 	"SWAP",
	OP,	0,	sizeof (opobj), c_sq,		"SQ",
	OP,	0,	sizeof (opobj), c_sqrt, 	"SQRT",
	OP,	0,	sizeof (opobj), c_pow,		"^",
	OP,	0,	sizeof (opobj), c_inv,		"INV",
	OP,	0,	sizeof (opobj), c_clear,	"CLEAR",
	OP,	0,	sizeof (opobj), c_pbegin,	"<<",   /*      \ these two tokens      */
	OP,	0,	sizeof (opobj), c_pend, 	">>",   /*      / must stay together    */
	OP,	0,	sizeof (opobj), c_eval, 	"EVAL",
	OP,	0,	sizeof (opobj), c_sto,		"STO",
	OP,	0,	sizeof (opobj), c_rcl,		"RCL",
	OP,	0,	sizeof (opobj), c_ift,		"IFT",
	OP,	0,	sizeof (opobj), c_ifte, 	"IFTE",
	OP,	0,	sizeof (opobj), c_gt,		">",
	OP,	0,	sizeof (opobj), c_ge,		">=",
	OP,	0,	sizeof (opobj), c_eq,		"==",
	OP,	0,	sizeof (opobj), c_le,		"<=",
	OP,	0,	sizeof (opobj), c_lt,		"<",
	OP,	0,	sizeof (opobj), c_ne,		"!=",
	OP,	0,	sizeof (opobj), c_dup,		"DUP",
	OP,	0,	sizeof (opobj), c_tron, 	"TRON",
	OP,	0,	sizeof (opobj), c_troff,	"TROFF",
	OP,	0,	sizeof (opobj), c_user, 	"USER",
	OP,	0,	sizeof (opobj), c_purge,	"PURGE",
	OP,	0,	sizeof (opobj), c_show, 	"SHOW",
	OP,	0,	sizeof (opobj), c_save, 	"SAVE",
	OP,	0,	sizeof (opobj), c_load, 	"LOAD",
	OP,	0,	sizeof (opobj), c_system,	"SYSTEM",
	OP,	0,	sizeof (opobj), c_if,		"IF",
	OP,	0,	sizeof (opobj), c_then, 	"THEN",
	OP,	0,	sizeof (opobj), c_else, 	"ELSE",
	OP,	0,	sizeof (opobj), c_endif,	"ENDIF",
	OP,	0,	sizeof (opobj), c_start,	"START",
	OP,	0,	sizeof (opobj), c_for,		"FOR",
	OP,	0,	sizeof (opobj), c_next, 	"NEXT",
	OP,	0,	sizeof (opobj), c_step, 	"STEP",
	OP,	0,	sizeof (opobj), c_do,		"DO",
	OP,	0,	sizeof (opobj), c_until,	"UNTIL",
	OP,	0,	sizeof (opobj), c_enddo,	"ENDDO",
	OP,	0,	sizeof (opobj), c_while,	"WHILE",
	OP,	0,	sizeof (opobj), c_repeat,	"REPEAT",
	OP,	0,	sizeof (opobj), c_endwhile,	"ENDWHILE",
	OP,	0,	sizeof (opobj), c_time, 	"TIME",
#ifdef __TURBOC__
	OP,	0,	sizeof (opobj), memmap, 	"MemMap",       /*      for debugging only      */
#endif
	OP,	0,	sizeof (opobj), c_ln,		"LN",
	OP,	0,	sizeof (opobj), c_exp,		"EXP",
	OP,	0,	sizeof (opobj), c_local,	"->",
	OP,	0,	sizeof (opobj), c_pi,		"PI",
	OP,	0,	sizeof (opobj), c_e,		"e",
	OP,	0,	sizeof (opobj), c_i,		"i",
	OP,	0,	sizeof (opobj), c_sin,		"SIN",
	OP,	0,	sizeof (opobj), c_cos,		"COS",
	OP,	0,	sizeof (opobj), c_tan,		"TAN",
	OP,	0,	sizeof (opobj), c_asin, 	"ASIN",
	OP,	0,	sizeof (opobj), c_acos, 	"ACOS",
	OP,	0,	sizeof (opobj), c_atan, 	"ATAN",
	OP,	0,	sizeof (opobj), c_c_r,		"C->R",
	OP,	0,	sizeof (opobj), c_im,		"IM",
	OP,	0,	sizeof (opobj), c_re,		"RE",
	OP,	0,	sizeof (opobj), c_r_c,		"R->C",
	OP,	0,	sizeof (opobj), c_bin,		"BIN",
	OP,	0,	sizeof (opobj), c_oct,		"OCT",
	OP,	0,	sizeof (opobj), c_dec,		"DEC",
	OP,	0,	sizeof (opobj), c_hex,		"HEX",
	OP,	0,	sizeof (opobj), c_print,	"PRINT",
	OP,	0,	sizeof (opobj), c_listend,	"}",
	OP,	0,	sizeof (opobj), c_b_r,		"B->R",
	OP,	0,	sizeof (opobj), c_r_b,		"R->B",
	OP,	0,	sizeof (opobj), c_maxr, 	"MAXR",
	OP,	0,	sizeof (opobj), c_minr, 	"MINR",
	OP,	0,	sizeof (opobj), c_abs,		"ABS",
	OP,	0,	sizeof (opobj), c_sign, 	"SIGN",
	OP,	0,	sizeof (opobj), c_ip,		"IP",
	OP,	0,	sizeof (opobj), c_fp,		"FP",
	OP,	0,	sizeof (opobj), c_floor,	"FLOOR",
	OP,	0,	sizeof (opobj), c_ceil, 	"CEIL",
	OP,	0,	sizeof (opobj), c_max,		"MAX",
	OP,	0,	sizeof (opobj), c_min,		"MIN",
	OP,	0,	sizeof (opobj), c_mod,		"MOD",

	OP,	0,	sizeof (opobj), c_dump,		"DUMP",
	OP,	0,	sizeof (opobj), c_version,	"VERSION",

};


const int noops = sizeof (ops) / sizeof (opobj);


int	traceflag = 0;

/*	constant objects	*/

complexobj	complex_zero	= {COMPLEX, 1, sizeof (complexobj), {0.0, 0.0}};
complexobj	complex_one	= {COMPLEX, 1, sizeof (complexobj), {1.0, 0.0}};
complexobj	complex_i	= {COMPLEX, 1, sizeof (complexobj), {0.0, 1.0}};

realobj 	real_zero	= {REAL, 1, sizeof (realobj), 0.0};
realobj 	real_e		= {REAL, 1, sizeof (realobj), M_E};
realobj 	real_pi 	= {REAL, 1, sizeof (realobj), M_PI};
realobj 	real_min	= {REAL, 1, sizeof (realobj), TINY_VAL};
realobj 	real_max	= {REAL, 1, sizeof (realobj), HUGE_VAL};

/*	stack backup for handling signals	*/

jmp_buf main_loop;
