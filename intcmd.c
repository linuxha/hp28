/****************************************************************

	Internally used commands

0.0	hjp	89-06-26

	initial version.

0.1	hjp	89-07-14

	traceflag added.

0.2	hjp	89-07-25

	OP added to destroy
	UNAME and STRING added.

0.3	hjp	89-08-15

	printobj now does not print to screen anymore but prints
	into iobuffer, which is then used by psr and c_save.

0.4	hjp	89-08-28

	ip moved out of interprete. Now global variable.

0.5	hjp	89-09-03

	findvar added.

0.6	hjp	89-09-03

	link field added.

0.7	hjp	89-10-04

	# of significant digits in printobj increased to 20.

0.8	hjp	89-11-23

	# of significant digits in printobj decreased to 15.
	binary object in printobj added.

0.9	hjp	89-12-02

	mallocobj added.

0.10	hjp	89-12-10

	fpehandler added.

0.11	hjp	90-02-27

	Comment object in printobj and destroy added.

0.12	hjp	90-03-02

	Lists in printobj added.

0.13	hjp	90-03-03

	malloc replaced by mallocobj where possible.

0.14	hjp	90-03-07

	inthandler added.

****************************************************************/


#ifdef __TURBOC__
#include <alloc.h>
#else
#include <malloc.h>
#endif

#include <math.h>
//#include <mem.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "misccmd.h"
#include "intcmd.h"
#include "debug.h"

char	* iop,					/*	pointer to current position in I/O buffer			*/
	* ioend = iobuffer + PROGMAXSIZE;	/*	pointer to end of I/O buffer - used to detect overflow		*/
						/*	this pointer should be before iobuffer to minimize 		*/
						/*	the danger of corruption by I/O buffer overflow			*/
char	iobuffer [PROGMAXSIZE];			/*	this buffer is used by the printobj routine			*/
						/*	so it's only a output buffer yet				*/

/*
	push object onto stack
*/

void push (genobj * p) {
  listobj * l;

  if ((l = mallocobj (LIST))) {
    p->link ++;

    l->link ++;
    l->obj = p;
    l->next = stack;
    stack = l;

  } else {
    lerror ("push", ERR_NOMEM, NULL);
  }
}

/*
	destroy object and free memory occupied by it
	if p points to a list:
		level == 0:
			destroy head of list and children, but not rest of list
			(used by c_drop ())
		level > 0:
			destroy whole list.
*/

void destroy (genobj * p, int level) {
	genobj * obj;
	genobj ** o;
	char	s[8];	/*	buffer for itoa	*/


	if (-- p->link) return;	/* do not destroy object physically if link count still > 0	*/

	switch (p->id) {
	case REAL:
	case COMPLEX:
	case QNAME:
	case UNAME:
	case STRING:
	case BINARY:
	case COMMENT:
	case DO:
	case WHILE:
	case FOR:
	case START:
		p->id = NOOBJ;
		free (p);
		break;
	case LIST:
		if (((listobj *)p)->obj) {
			destroy (((listobj *)p)->obj, level + 1);
		}

		obj = (genobj *)((listobj *) p)->next;
		p->id = NOOBJ;
		free (p);
		if (level && obj) destroy (obj, level + 1);
		break;
	case PROGRAM:
		o = (genobj **) (p + 1) - 1;
		do {
			o ++;
			if ((* o)->id != OP) {
				destroy (* o, level + 1);
			}
		} while ((* o)->id != OP || ((opobj *) (* o))->fptr != c_pend);
		p->id = NOOBJ;
		free (p);
		break;
	case OP:
		break;
	case VARIABLE:
		destroy (((varobj *)p)->val, 1);
		p->id = NOOBJ;
		free (p);
		break;
	default:
		lerror ("destroy", INT_NXOBJ, itoa (p->id, s, 16));
	}
}

genobj *
duplicate (genobj * obj, int level) {
	genobj	* new;
	genobj	** oo, ** on;

	switch (obj->id) {
	case REAL:
	case COMPLEX:
	case QNAME:
	case UNAME:
	case STRING:
		new = malloc (obj->size);
		if (new) {
			memcpy (new, obj, obj->size);
			new->link = 0;
		}
		break;
	case LIST:
		if (new = mallocobj (LIST)) {

			((listobj *)new)->obj	= duplicate (((listobj *)obj)->obj, 1);

			if (level && obj) {
				((listobj *)new)->next	= duplicate (((listobj *) obj)->next, 1);
			}
		}
		break;
	case PROGRAM:
		if (new = malloc (obj->size)) {

			new->id = PROGRAM;
			new->link = 0;
			new->size = obj->size;

			oo = (genobj **) (obj + 1) - 1;
			on = (genobj **) (new + 1) - 1;
			do {
				oo ++; on ++;
				if ((* oo)->id != OP) {
					* on = duplicate (* oo, 1);
				} else {
					* on = * oo;
				}
			} while ((* oo)->id != OP || ((opobj *) (* oo))->fptr != c_pend);
		}
		break;
	default:
		lerror ("duplicate", INT_NXOBJ, NULL);
		new = NULL;
	}
	if (! new) {
		lerror ("duplicate", ERR_NOMEM, NULL);
	}
	return new;
}

void
printobj (genobj * obj) {
	genobj ** o;

	switch (obj->id) {
	case REAL:
		sprintf (iop, "%.15lg ", ((realobj *)obj)->val);
		break;
	case COMPLEX:
		sprintf (iop, "(%.15lg, %.15lg) ", ((complexobj *)obj)->val.x, ((complexobj *)obj)->val.y);
		break;
	case OP:
		sprintf (iop, "%s ", ((opobj *) obj)->name);
		break;
	case PROGRAM:
		sprintf (iop, "<< "); iop += 3;
		o = (genobj **) (obj + 1) - 1;
		do {
			o ++;
			printobj (* o);
		} while ((* o)->id != OP || ((opobj *) (* o))->fptr != c_pend);
		break;
	case QNAME:
		sprintf (iop, "'%s' ", ((nameobj *)obj)->name);
		break;
	case UNAME:
		sprintf (iop, "%s ", ((nameobj *)obj)->name);
		break;
	case STRING:
		sprintf (iop, "\"%s\" ", ((stringobj *) obj)->val);
		break;
	case BINARY:
		* iop ++ = '#';
		ultoa (((binaryobj *) obj)->val, iop, radix);
		iop = strchr (iop, 0);
		switch (radix) {
		case 2:
			strcpy (iop, "b ");
			break;
		case 8:
			strcpy (iop, "o ");
			break;
		case 10:
			strcpy (iop, "d ");
			break;
		case 16:
			strcpy (iop, "h ");
			break;
		default:
			sprintf (iop, "(%d) ", radix);
			break;
		}
		break;
	case COMMENT:
		sprintf (iop, "\t;%s\n\t", ((commentobj *) obj)->val);
		break;
	case LIST:
		sprintf (iop, "{ "); iop += 2;
		while (((listobj *) obj)->obj) {
			printobj (((listobj *) obj)->obj);
			obj = ((listobj *) obj)->next;
		}
		sprintf (iop, "} "); iop += 2;
		break;
	default:
		sprintf (iop, "System Object ");
		break;
	}
	iop = strchr (iop, 0);
	if (iop >= ioend) {
		lerror ("printobj", INT_BUFOVER, NULL);
		exit (1);
	}
}

void
psr (listobj * l, int n) {
	if (l->next) {
		psr (l->next, n + 1);
	}
	iop = iobuffer;
	printobj (l->obj);
	printf ("%d:\t%s\n", n, iobuffer);
}

void
printstack (void) {
	printf ("---------------------------------------\n");
	if (stack) {
		psr (stack, 1);
	}
#ifdef __TURBOC__
	printf ("free: %lu, stack: %u\n", coreleft (), _SP);
#endif
	printf ("---------------------------------------\n");
}

varobj * findvar (char * name)
{
	varobj * p;

	for (p = localvars; p && strcmp (name, p->name); p = p->next);
	if (! p) for (p = vars; p && strcmp (name, p->name); p = p->next);
	return p;
}

void interprete (genobj * obj, int level)
{
	if (traceflag) {
		printstack ();
		printf ("now executing : ");
		iop = iobuffer;
		printobj (obj);
		printf ("%s\n", iobuffer);
		printf ("\n");
	}

	switch (obj->id) {
	case OP:
		(((opobj *) obj)->fptr) ();
		break;

	case UNAME:
		{
                	varobj	* p;

			p = findvar (((nameobj *)obj)->name);
			if (p) {
				interprete (p->val, 0);
			} else {
				lerror ("interprete", ERR_NXVAR, ((nameobj *)obj)->name);
			}
		}
		break;

	case COMMENT:
		break;

	case PROGRAM:
		if (level == 0) {
			genobj ** ip_back = ip;

			ip = (genobj **) (obj + 1) - 1;
			do {
				ip ++;
				interprete (* ip, 1);
			} while ((* ip)->id != OP || ((opobj *) (* ip))->fptr != c_pend);
			ip = ip_back;
			break;
		}
		/*	break intentionally omitted:
			if (level != 0) push !
		*/

	default:
		push (obj);
	}
}


/*
	mallocobj	--	allocate and initialize object.
*/

genobj
*mallocobj (int type) {
	genobj	* p;
	static
	size_t	sizes [] = {
		0,
		sizeof (realobj),	/*	1           	*/
		sizeof (complexobj),	/*	2	        */
		sizeof (stringobj),	/*	3	        */
		sizeof (genobj),	/*                      */
		sizeof (genobj),	/*                      */
		sizeof (genobj),	/*                      */
		sizeof (genobj),	/*                      */
		sizeof (listobj),	/*	8	        */
		sizeof (nameobj),	/*	9	QNAME	*/
		sizeof (nameobj),	/*	10	UNAME	*/
		sizeof (opobj),		/*	11	        */
		sizeof (genobj),	/*                      */
		sizeof (binaryobj),	/*	13	        */
		0,
		0,
		sizeof (varobj),	/*	16	        */
		sizeof (loopobj),	/*	17	START	*/
		sizeof (loopobj),	/*	18	FOR	*/
		sizeof (loopobj),	/*	19	DO	*/
		sizeof (loopobj),	/*	20	WHILE	*/
	};

	char	s [6];

	if (type > WHILE || type <= 0)
	{
		lerror ("mallocobj", INT_NXOBJ, itoa (type, s, 16));
		return NULL;
	}
	if (p = malloc (sizes [type])) {
		p->id = type;
		p->link = 0;
		p->size = sizes [type];
	}
	return p;
}

/*
	fpehandler -- Floating Point Exception handler

	Just a dummy to inhibit program termination.
	Prints out an lerror message.
	Results of the operation where the exception occured are undefined!
*/

void fpehandler (int sig) {
	/* 	reinstall handler	*/
	signal (sig, fpehandler);

	lerror ("fpehandler", ERR_FPE);
}

/*
	inthandler -- handles SIGINT (ctrl-C) signals
*/

void inthandler (int sig) {
	/*	reinstall handler	*/
	signal (sig, inthandler);

	printf ("SIGINT received -- returning to main loop\n");

	longjmp (main_loop, sig); // void longjmp(jmp_buf env, int val);
}
