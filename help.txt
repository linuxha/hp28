		  RPL -- AN HP-28 EMULATOR


Introduction
~~~~~~~~~~~~

RPL is an interpreter for Hewlett Packard's RPL-Language. In
its final stage it should have the same functionality as
HP's HP28 calculator series plus the advantages a PC has
over a calculator (larger screen and better graphics, a
better keyboard, a filesystem and more speed).

This version of RPL is an alpha-release. It contains only a
little subset of the functionality, and the user interface
is ugly (and user-unfriendly), but convenient for debugging.

I release it at this early stage for two reasons:

o	I want as many people as possible to play around
	with it and tell me their opinion. I am especially
	interested in the following issues:

	o	Which functions do you miss most ?
		If you are an HP28-User, what are you doing
		with the HP28, what you would do with the PC
		if you had the right program?

	o	What should the user interface look like?
		Two solutions seem possible. A curses-based
		interface would provide maximum portability,
		but prevents the use of graphics.
		If I use Turbo-C's graphics a really cute UI
		could be written (combined command-line and
		mouse, for all the IBM vs. Mac-flamers), but
		porting it to some other environment could
		be difficult.

		In any case I would like different windows
		for the stack, the command line/editor,
		output and Menues/Help.

	o	Do you want the keywords in upper case (like
		they are in the HP28 and the current
		version) or in lower case (my favorite) or
		do you want the program to be
		case-insensitive (I hope that does not start
		another flame-war)

	o	Do you like the conversion rules?
		binary + Real -> binary seems a little
		strange to me.

o	I would be grateful for any comments on my code. If
	you have ideas how to make something faster or more
	elegant, share them. If you even want to implement
	some functions you miss, you're welcome. In any case
	I have nothing to offer but honor. :-)


A Quick Overview
~~~~~~~~~~~~~~~~

After starting the program you will get some messages
reading ``clearmem: xxxxx bytes at ssss:oooo'' which you can
ignore and an IBMish-looking prompt. You can now enter your
operands and operators (remember to use RPN) and after
pressing return, they will be evaluated and the stack will
be shown.

----------------------------------------------------------------
Example:
	===> 2 <cr>		The real number two is
	----------------	evaluated, i.e. pushed
	1:	2		onto the stack.
	----------------
	===> 3 <cr>		Same for 3.
	----------------
	2:	2
	1:	3
	----------------
	===> - <cr>		- takes the two topmost
	----------------	objects from the stack and
	1:	-1		pushes the result.
	----------------

	The same result by a little faster way:

	===> 2 3 - <cr>
	----------------
	1:	-1
	----------------
----------------------------------------------------------------

Of course real numbers are not the only data type RPL can
handle.  You can also use complex numbers, binaries, strings
and lists.  Vectors and matrices are not yet implemented,
but will be added soon.

----------------------------------------------------------------
Examples:
	(2,-3.5)		The complex number 2 - 3.5i
	#100d			The binary 100(base 10)
	#ah			The binary A(base 16) = 10
	#101			The binary 101 in the
				current base.
	"Hello, world!"         A string
	{ 2 (3,5) { 2 'A' } }   A list containing various objects.
----------------------------------------------------------------

Strings are entered as in C, so you can use \ as an escape
character. In addition to the classical C escape sequences
\x<hexdigits> was added to allow entering characters in hex
(instead of the rather strange octal notation obviously
preferred by language designers like Ritchie or Wirth).

Currently there is no support for {} (arrays).

Objects can be stored in variables. A variable name can be
max. 31 characters long and may consist of all characters
except `'' and ` '.  If a variable name is found, it is
evaluated, i.e. replaced by the value of the variable. To
prevent this if you need the variable name itself, just
preceed it with a `''.

----------------------------------------------------------------
Examples:
	2 'foo STO      stores 2 in variable foo
	foo		pushes the value of foo (2) onto the
			stack.
	'foo RCL        does the same.
----------------------------------------------------------------

To write a program just inclose the sequence of commands you
would normally use in `<<' and `>>'.

----------------------------------------------------------------
Examples:
	To compute the hypothenuse from the two cathetes of
	a triangle you could write:

	<< SQ SWAP SQ + SQRT >>

	If you store this program in a variable named HYP
	you create a new command HYP.

	If you find the program above confusing, you can use
	local variables to make it clearer:

	<< -> a b << a SQ b SQ + SQRT >> >>

----------------------------------------------------------------

There is something strange about the -> operator:  It is not
purely postfix.  It first looks how many variable names are
following it, then takes an appropriate number of objects
from the stack and stores them in the variables.  Then it
evaluates the program coming after the variable names and
destroys all the variables again.

There are some other operators which do not operate on the
stack only but also on the program in which they reside
(they can only be used in programs, not directly).
In other languages they are keywords used to give the
program a structure and are compiled into jumps, but in RPL
they are just operators with a slightly irregular behaviour.

----------------------------------------------------------------
Example:
	The not yet implemented SIGN function could be
	implemented as:

	<< -> a
	  << IF a < 0 THEN
	       -1
	     ELSE
	       IF a > 0 THEN
		 1
	       ELSE
		 0
	       ENDIF
	     ENDIF
	   >>
	>>
----------------------------------------------------------------

Similar to [IF] THEN [ELSE] ENDIF there exist
START (STEP|NEXT), FOR (STEP|NEXT), WHILE REPEAT ENDWHILE
and DO [UNTIL] ENDDO.

HP28-users will note that I replaced the uniform END with
different words for each construct.  In this way the parser
does not need to know that they are something special, but
can treat them as normal operators.

Comments start with a semicolon and take the rest of the
line.  This means that comments cannot be entered at the
command line, but only in files which can then be loaded
with LOAD.  This is the recommended way of writing programs
anyway, because they can be easily edited, nicely formatted
and they are not limited by DOS's 128 character command
line.


Functions
~~~~~~~~~

These are the functions that are already implemented.
After each function the possible arguments and results are
listed.

For example, ``SQRT: real->real,  real->complex,
complex->complex'' means that SQRT always takes one
argument, which can be real or complex. a complex argument
always yields a complex result, but a real argument can
either yield a real (if the argument was >= 0), or a complex
result.

!=:	real,real->real

	returns 0, if arguments are equal, else 1.

*:	int,int->int,	int,real->int,	real,int->int,
	real,real->real,	real,complex->complex,
	complex,real->complex,	complex,complex->complex

	returns product of arguments.

+:	int,int->int,	int,real->int,	real,int->int,
	real,real->real,	real,complex->complex,
	complex,real->complex,	complex,complex->complex

	returns sum of arguments.

-:	int,int->int,	int,real->int,	real,int->int,
	real,real->real,	real,complex->complex,
	complex,real->complex,	complex,complex->complex

	returns argument in level 2 minus arg. in level 1.

->:	->

	creates local variables and initializes them
	with values from the stack. Described in more detail
	above.

/:	int,int->int,	int,real->int,	real,int->int,
	real,real->real,	real,complex->complex,
	complex,real->complex,	complex,complex->complex

	returns argument in level 2 minus arg. in level 1.

<:	real,real->real

	1 if arg. in level 2 less than ar. in level 1, else
	0.

<<:
	Start of program.

<=:	real,real->real

	1 if arg. in level 2 less or equal to arg. in level
	1, else 0.

==:	real,real->real

	1 if arg. in level 2 equal to arg. in level 1, else 0.

>:	real,real->real

	1 if arg. in level 2 greater than arg. in level
	1, else 0.

>=:	real,real->real

	1 if arg. in level 2 greater or equal to arg. in
	level 1, else 0.

>>:
	End of program.

ABS:	real->real,	complex->complex
	Absolute value of argument.

ACOS:	real->real,	complex->complex
	Arcus cosine of argument.

ASIN:	real->real,	complex->complex
	Arcus sine of argument.

ATAN:	real->real,	complex->complex
	Arcus tangent of argument.

BIN:
	Set default base for binaries to 2.

B->R:	binary->real

	convert binary to real.

C->R:	complex->real,real
	Split complex number in real and imaginary part.

CEIL:	real->real
	Smallest integer greater than argument.

CLEAR:	stack->
	Clears the whole stack.

COS:	real->real,	complex->complex
	Cosine of argument.

DEC:
	Set default base for binaries to 10.

DO:
	Part of DO-UNTIL-ENDDO loop.

DROP:	object->

	Drop argument from stack.

DUP:	object->object,object

	Duplicate argument.

e:	->real

	Push the value of e (2.71828...)

ELSE:
	Part of IF-THEN-ELSE-ENDIF construct.

ENDDO:
	Part of DO-UNTIL-ENDDO loop.

DO:

	Part of DO-UNTIL-ENDDO loop.

ENDIF:

	Part of IF-THEN-ELSE-ENDIF construct.

ENDWHILE:

	Part of WHILE-REPEAT-ENDWHILE construct.

EVAL:	object->?

	Evaluate object.

EXP:	real->real,	real->complex

	Compute e to the power of argument.

FLOOR:	real->real

	Greatest integer smaller than argument.

FOR:	real,real->

	Part of FOR-(NEXT|STEP) construct.

FP:	real->real

	Fractional part of argument.

HEX:

	Set default base to 16.

i:	->complex

	Push value of i ((0, 1)).

IF:

	Part of IF-THEN-ELSE-ENDIF construct.

IFT:	real,object->object

	evaluate arg. in level 1 if argument in level 2 is
	non-zero.

IFTE:	real,object,object->object

	evaluate arg. in level 2 if argument in level 3 is
	non-zero, else evaluate argument in level 1.

IM:	complex->real

	Get imaginary part of complex number.

INV:	real->real,	complex->complex

	Reciprocal of argument.

IP:	real->real

	Integer part of argument.

LN:	real->real,	real->complex,	complex->complex

	natural logarithm of argument.

LOAD:	string->?

	The argument is treated as a file name. The file is
	read into the command line, which is then parsed as
	if entered from the keyboard.

MAX:	real,real->real

	Maximum of two numbers.

MAXR:	->real

	Maximum real number.

MIN:	real,real->real

	Minimum of two numbers.

MINR:	->real

	Minimum positive real number.

MOD:	real,real->real

	Compute arg. in level 2 modulo arg. in level 1.
	Defined as:

	x mod y = x - y * floor (x / y)

	This definition ensures that
	sign (x mod y) = sign (y), which is more practical
	than the definition used in most programming
	languages, where sign (x mod y) = sign (x).

NEG:	real->real,	complex->complex

	Multiplication with -1.

NEXT:

	Part of FOR-NEXT construct.

OCT:

	Set default base to 8.

OFF:

	Leave program.

PI:	->real

	Push the value of pi (3.14159..)

POW:	real,real->real

	raise arg in level 2 to the power of arg in level 1.

PRINT:	object->

	print object to standard output.

PURGE:	qname->,	string->

	Purge variable of file.

R->B:	real->binary

	Convert real to binary.

R->C:	real,real->complex

	Combine two reals to a complex number.

RCL:	qname->object

	Push the value of a variable onto the stack, but do
	NOT evaluate it.

RE:	complex->real

	Get the real part of a complex number.

REPEAT: real->

	Part of WHILE-REPEAT-ENDWHILE.

SAVE:	object,string->

	Append object to file.

SHOW:

	Show a list of builtin commands

SIGN:	real->real,	complex->complex.

	Sign of argument.
	If the argument is zero, the sign is zero, too.
	If not the sign is defined as: arg / abs(arg).

SIN:	real->real,	complex->complex

	Sine of argument.

SQ:	real->real,	complex->complex

	Sqare argumnet.

SQRT:	real->real,	real->complex,	complex->complex

	Sqare root of argument.

START:	real,real->

	Part of START-(NEXT|STEP)

STEP:	real->

	Part of (FOR|START)-STEP

STO:	object,qname->

	Store object in variable

SWAP:	object,object->object,object

	Swap two topmost objects on stack.

SYSTEM: string->

	spawn a shell to execute string.

TAN:	real->real,	complex->complex

	Tangent of argument.

THEN:	real->

	Part of IF-THEN-ELSE-ENDIF

TIME:	->real

	Time in seconds since 1970-01-01 00:00 GMT

TROFF:

	Trace on.

TRON:

	Trace off.

UNTIL:

	Part of DO-UNTIL-ENDDO.

USER:

	List all user variables.

WHILE:

	Part of WHILE-REPEAT-ENDWHILE.


PS
~~

Sorry for the terse docu. If you need help, try looking at
the comments in the program, find somebody who has an HP28
or send me mail.
