/****************************************************************

	Commands related to real objects.

0.0	hjp	90-03-04

	initial version.

****************************************************************/

#ifndef I_realcmd

	#define I_realcmd

	void	c_minr	(void);
	void	c_maxr	(void);
	void	c_abs	(void);
	void	c_sign	(void);
	void	c_mod	(void);
	void	c_max	(void);
	void	c_min	(void);
	void	c_floor	(void);
	void	c_ceil	(void);
	void	c_ip	(void);
	void	c_fp	(void);

#endif
