/****************************************************************

	Functions used for implementing user logarithmic commands

0.0	hjp	89-12-03

	initial version
	LN and EXP extracted from ArithCmd.

****************************************************************/

#ifndef I_logcmd

	#define I_logcmd

	void	c_exp	(void);
	void	c_ln	(void);
#endif
