/****************************************************************

Module:
	Errors

Description:
	Error messages


Modification history:

	0.0	hjp	89-06-26

		initial version

	0.1	hjp	89-07-25

		ERR_NXVAR added

	0.2	hjp	89-08-15

		parameter detail added to error
		ERR_DOS added.

	0.3	hjp	89-08-29

		ERR_LOOP added.

	0.4	hjp	89-10-05

		INT_BADLINK added.
		error has now variable parameter list.

	0.5	hjp	89-11-15

		parameter to ERR_SYNTAX added.

	0.6	hjp	89-12-11

		ERR_FPE added.

	0.7	hjp	90-02-27

		START, FOR, DO, WHILE, COMMENT added to id2str.

****************************************************************/

#define ERRORS_C

#include <port.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "errors.h"
#include "debug.h"

char *errstr [] = {
  "No error\n",
  "Wrong argument type %s\n",
  "Stack empty\n",
  "Too few arguments\n",
  "Syntax error: %s\n",
  "Out of Memory\n",
  "No such variable: '%s'\n",
  "No user variables\n",
  "Error reported by OS: %s\n",
  "Loop nesting error\n",
  "Floating Point Exception\n",
  "Error reported by OS: %s\n",

  "(INTERNAL) stack is not a list\n",
  "(INTERNAL) unknown object type %s\n",
  "(INTERNAL) impossible link count %s",

  "(PANIC) buffer overflow -- committing suicide ...\n",
};

char *id2str(int id) {
	char *rc;

	switch (id) {
	case REAL:
		rc = "REAL";
		break;
	case COMPLEX:
		rc = "COMPLEX";
		break;
	case BINARY:
		rc = "BINARY";
		break;
	case PROGRAM:
		rc = "PROGRAM";
		break;
	case OP:
		rc = "OP";
		break;
	case UNAME:
		rc = "UNAME";
		break;
	case QNAME:
		rc = "QNAME";
		break;
	case STRING:
		rc = "STRING";
		break;
	case LIST:
		rc = "LIST";
		break;
	case VARIABLE:
		rc = "VARIABLE";
		break;
	case START:
		rc = "START";
		break;
	case DO:
		rc = "DO";
		break;
	case FOR:
		rc = "FOR";
		break;
	case WHILE:
		rc = "WHILE";
		break;
	case COMMENT:
		rc = "COMMENT";
		break;
	default:
		rc = NULL;
	}

	return rc;
}

char*
ncpy(char *dest, const char *src, size_t n){
  size_t i;

  for (i = 0 ; i < n && src[i] != '\0' ; i++)
    dest[i] = src[i];
  for ( ; i < n ; i++)
    dest[i] = '\0';
  
  return dest;
}

#ifndef MAXLINE
#define MAXLINE 256
#endif

void
lerror (const char *cmd, int errnum, ... ) {
  char line[MAXLINE+1];
  char str[256];
  char *ptr;

  va_list ap;

  ptr = line;

  ncpy(str, cmd, 255);

  va_start(ap, errnum);

  //  printf("CMD: %s\nErr#: %d\n%s\n", str, errnum, errstr[errnum]);
  // printf("%s: \n", str);

  vsnprintf(ptr, MAXLINE, errstr[errnum], ap);
  printf("%s", ptr);
  va_end(ap);

}
/*

void lerror (char * cmd, int errnum, ... ) {
  va_list argptr;

  printf ("error (%s, %d, ...) {\n", cmd, errnum);

  va_start (argptr, errnum);
  printf ("%s: ", cmd);
  vprintf (errstr [errnum], argptr);
  va_end (argptr);

  printf ("} error\n");
}
*/

char *estr [] = {
  "%s: No error\n",
  "%s: Wrong argument type %s\n",
  "%s: Stack empty\n",
  "%s: Too few arguments\n",
  "%s: Syntax error: %s\n",
  "%s: Out of Memory\n",
  "%s: No such variable: '%s'\n",
  "%s: No user variables\n",
  "%s: Error reported by OS: %s\n",
  "%s: Loop nesting error\n",
  "%s: Floating Point Exception\n",
  "%s: File (%s) error: %s\n",

  "%s: (INTERNAL) stack is not a list\n",
  "%s: (INTERNAL) unknown object type %s\n",
  "%s: (INTERNAL) impossible link count %s",

  "%s: (PANIC) buffer overflow -- committing suicide ...\n",
};


void
eprintf(const char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);

  vprintf(fmt, ap);

  va_end(ap);
}
