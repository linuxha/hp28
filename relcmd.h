/****************************************************************

Module:
	RelCmd

Description:
	Commands implementing relational operations


Modification history:

	0.0	hjp	89-07-14

		initial version

****************************************************************/

#ifndef I_relcmd

	#define I_relcmd

	void c_gt (void);
	void c_ge (void);
	void c_eq (void);
	void c_le (void);
	void c_lt (void);
	void c_ne (void);

#endif
