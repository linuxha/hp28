/****************************************************************

	Store commands

0.0	hjp	89-07-08

	initial version

0.1	hjp	89-08-14

	PURGE and USER added

****************************************************************/

#ifndef I_storecmd

	#define I_storecmd

	void	c_sto	(void);
	void	c_rcl	(void);
	void	c_purge	(void);
	void	c_user	(void);
	void	c_show	(void);

#endif
