/****************************************************************

Module:
	HP main module

Description:
	interactive loop


Modification history:

	0.0	hjp	89-06-26

		initial version

	0.1	hjp	89-07-25

		main greatly simplified by replacement of switch (obj->id)
		through interprete (obj, 1);

	0.2	hjp	89-12-11

		FPE handling added.

	0.3	hjp	90-03-06

		Stack length, profiling and clearmem excluded for 
		non-Turbo-C environment.

	0.4	njc	12-09-03

		Yes, I've been using this for more than 23 years
		(with various modifications to keep it somewhat
		up to date). Now I'd like to modify it further
		to make it more Unix friendly. ;-)

	0.5	njc	12-09-03

		Added readline, history and initial default rpl
		load support. Hope to add futher support for ENV
		variables in file access routines
****************************************************************/


#include <signal.h>
#include <stdio.h>
#include <string.h>

#ifdef READLINE
#include <stdlib.h>		// needed by malloc
extern char *rdptr;
#endif

#include "rpl.h"
#include "globvar.h"
#include "intcmd.h"
#include "parser.h"
#include "debug.h"

#include "errors.h"

#ifdef __TURBOC__
uint _stklen = 0x4000;	/*	16 k Bytes of stack	*/
#endif

int
main (int argc, char ** argv) {
	genobj * obj;

#ifdef __TURBOC__
	if (! strcmp (argv [1], "-p")) {
		prof_start (argv [0]);
	}
#endif

	/*	clear memory so that MemMap will work right	*/

#ifdef __TURBOC__
	clearmem (0x8000);
#endif

	/*	set up floating point exception handler	*/

	signal (SIGFPE, fpehandler);
	signal (SIGINT, inthandler);

#ifdef READLINE
extern void *malloc(size_t size);
extern char *getenv(const char *name);
extern int sprintf(char *str, const char *format, ...);

	// This bizarreness is just so I can free cmdline before readline()
	cmdline = malloc(PROGMAXSIZE);

	// Load the default user programs
	sprintf(cmdline, "\"%s/.hp28/default.rpl\" LOAD", getenv("HOME"));
	rdptr = cmdline; empty = 0;			/*	simulate edit ()		*/

	obj = getobj();
	if (obj) {
	  interprete (obj, 1);
	  obj->link ++;		/* destroy original instance of object	*/
	  destroy (obj, 1);
	} else {
	  empty = 1;
	  printstack ();
	}
#endif

	for (;;) {
		setjmp (main_loop);
		obj = readvalue ();
		if (obj) {
			interprete (obj, 1);
			obj->link ++;		/* destroy original instance of object	*/
			destroy (obj, 1);
		} else {
			empty = 1;
			printstack ();
		}
	}
}

