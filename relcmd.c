/****************************************************************

Module:
	RelCmd

Description:
	Commands implementing relational operations


Modification history:

0.0	hjp	89-07-14

	initial version

0.1	hjp	89-09-04

	destroy added after push to prevent memory loss.

0.2	hjp	89-10-01

	link count added. destroy now superfluos - removed.

0.3	hjp	90-03-03

	malloc replaced by mallocobj (at last!).

0.4	hjp	90-03-07

	argument checking fixed in all functions.

****************************************************************/

#include "rpl.h"
#include "relcmd.h"
#include "globvar.h"
#include "errors.h"
#include "intcmd.h"
#include "stackcmd.h"

#include "debug.h"

/*
	Level 2 > Level 1 ?
*/

void c_gt (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror (">", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val > ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror (">", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror (">", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	Level 2 >= Level 1 ?
*/

void c_ge (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror (">=", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val >= ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror (">=", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror (">=", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	Level 2 == Level 1 ?
*/

void c_eq (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("==", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val == ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror ("==", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("==", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	Level 2 <= Level 1 ?
*/

void c_le (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("<=", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val <= ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror ("<=", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("<=", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	Level 2 < Level 1 ?
*/

void c_lt (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("<", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val < ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror ("<", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("<", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	Level 2 != Level 1 ?
*/

void c_ne (void)
{
	genobj * a, * b, * c;

	if (! stack || ! stack->next) {
		lerror ("!=", ERR_2FEWARG);
		return;
	}

	b = stack->obj; a = stack->next->obj;

	if (a->id == REAL && b->id == REAL) {
		c = mallocobj (REAL);
		((realobj *)c)->val = ((realobj *) a)->val != ((realobj *) b)->val;
		c_drop ();
		c_drop ();
		push (c);
	} else {
		if (a->id != REAL) lerror ("!=", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("!=", ERR_WRTYPE, id2str (b->id));
	}
}
