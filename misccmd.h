/****************************************************************

	Miscellaneous user commands

0.0	hjp	89-06-27

	initial version

0.1	hjp	89-07-14

	TRON, TROFF added

0.2	hjp	89-08-29

	TIME added

0.3	hjp	89-10-04

	-> (c_local) added

0.4	hjp	89-11-23

	BIN, OCT, DEC, HEX added.

0.5	hjp	90-03-02

	} added.

****************************************************************/

#ifndef I_misc_cmd

	#define I_misc_cmd

	void	c_bin		(void);
	void	c_dec		(void);
	void	c_hex		(void);
	void	c_listend	(void);		/*	}	*/
	void	c_oct		(void);
	void	c_off		(void);
	void	c_pbegin 	(void);
	void	c_pend		(void);
	void	c_eval		(void);
	void	c_tron		(void);
	void	c_troff 	(void);
	void 	c_time		(void);
	void	c_local		(void);		/*	->	*/

#endif
