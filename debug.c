/****************************************************************

Module:
	Debug

Description:
	Contains variables of all types to enable casts in the debugger.
	Contains functions used for debugging.

	WARNING! This module is very compiler-dependent. If you are not
		using Turbo C 2.0 on an IBM PC-compatible, you will have
		to change it or simply throw it out.

Modification history:

	0.0	hjp	89-07-08

		initial version

	0.1	hjp	89-07-25

		#include <stdio.h> added to supress warnings

	0.2	hjp	89-08-28

		stringobjs added

	0.3	hjp	89-08-28

		opobjs added

	0.4	hjp	89-09-04

		memmap added.
		clearmem added.

	0.5	clearmem improved. 
		uses variable blocksize now.

****************************************************************/

#include <alloc.h>
#include <mem.h>
#include <stdio.h>

#include "rpl.h"
#include "errors.h"

nameobj	no;
nameobj	* nop;
stringobj	so;
stringobj	* sop;
opobj	oo;
opobj	* oop;
realobj	ro;
realobj	* rop;

void * debugmalloc (unsigned n)
{
	void * p = malloc (n);

	printf ("allocated %u bytes at %p\n", n, p);
	return p;
}

void	debugfree (void * p)
{
	printf ("freeing %lu bytes at %p\n", ((long *) p)[-2] - 1, p);

	free (p);
}

void * debugrealloc (void * p, unsigned n)
{
	void * p1;

	printf ("reallocating %lu bytes at %p ", ((long *) p)[-2] - 1, p);
	p1 = realloc (p, n);
	printf ("to %u bytes at %p\n", n, p1);
	return p1;
}

void memmap (void)
{
	uint	seg;
	uint	* p;
	char	* s;

	for (seg = _SS + (_stklen >> 4); seg < 0xA000; seg ++) {

		p = (uint *) (((long) seg << 16) + 0x0008);
		if (s = id2str (* p)) {
			printf ("%x: %s (link = %u, size = %u)\n", seg, s, p [1], p [2]);
		}
	}
}


/*
	clearmem

	Allocate as much memory as possible, clear it, and release it again.
*/

void clearmem (uint chunk)
{
	void * p;

	if (chunk >= 16) {
		if (p = malloc (chunk - 8)) {
			printf ("clearmem: %d bytes at %p\n", chunk - 8, p);
			memset (p, 0, chunk - 8);
			clearmem (chunk);
			free (p);
		} else {
			chunk >>= 1;
			clearmem (chunk);
		}
	}
}
