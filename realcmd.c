/****************************************************************

	RealCmd	--	Commands for manipulating real objects.
			(The REAL menu on HP28)

0.0	hjp	90-03-04

	initial version

****************************************************************/

#include <stdlib.h>

#include "arithcmd.h"
#include "errors.h"
#include "globvar.h"
#include "intcmd.h"
#include "realcmd.h"
#include "rpl.h"
#include "stackcmd.h"


/*
	MAXR	--	push maximum real number

	-> REAL
*/

void	c_maxr (void)
{
	push (&real_max);
}


/*
	MINR	--	push minimum positive real number

	-> REAL
*/

void	c_minr (void)
{
	push (&real_min);
}


/*
	ABS	--	absolute value of argument

	REAL	->	REAL
	COMPLEX	->	REAL
*/

void c_abs (void)
{
	genobj		* a;
	realobj 	* c;

	if (! stack) {
		lerror ("ABS", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("ABS", ERR_NOMEM);
			return;
		}
		((realobj *)c)->val = fabs(((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {

		if (!(c = mallocobj (REAL)))
		{
			lerror ("ABS", ERR_NOMEM);
			return;
		}
		c->val = mycabs (((complexobj *) a)->val);
		c_drop ();
		push (c);
	} else {
		lerror ("ABS", ERR_WRTYPE, id2str (a->id));
	}
}


/*
	SIGN	--	sign of argument

	REAL	->	REAL
	COMPLEX	->	COMPLEX
*/

void c_sign (void)
{
	genobj		* a;

	if (! stack) {
		lerror ("SIGN", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL && ((realobj *)a)->val == 0.0) {
		c_drop ();
		push (& real_zero);
	} else if (a->id == COMPLEX && ((complexobj *) a)->val.x == 0.0 && ((complexobj *) a)->val.y == 0.0) {
		c_drop ();
		push (& complex_zero);
	} else {
		c_dup ();
		c_abs ();
		c_div ();
	}
}


/*
	IP	--	integer part of argument

	REAL	->	REAL
*/

void c_ip (void)
{
	genobj		* a;
	realobj		* r;
	double		x;

	if (! stack) {
		lerror ("IP", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("IP", ERR_NOMEM);
			return;
		}
		modf (((realobj *) a)->val, & x);
		((realobj *)r)->val = x;
		c_drop ();
		push (r);
	} else {
		lerror ("IP", ERR_WRTYPE, id2str (a->id));
	}
}




/*
	FP	--	fractional part of argument

	REAL	->	REAL
*/

void c_fp (void)
{
	genobj		* a;
	realobj		* r;
	double		x;

	if (! stack) {
		lerror ("FP", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("FP", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = modf (((realobj *) a)->val, & x);
		c_drop ();
		push (r);
	} else {
		lerror ("FP", ERR_WRTYPE, id2str (a->id));
	}
}




/*
	FLOOR	--	round to nearest integer smaller than argument.

	REAL	->	REAL
*/

void c_floor (void)
{
	genobj		* a;
	realobj		* r;

	if (! stack) {
		lerror ("FLOOR", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("FLOOR", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = floor (((realobj *) a)->val);
		c_drop ();
		push (r);
	} else {
		lerror ("FLOOR", ERR_WRTYPE, id2str (a->id));
	}
}




/*
	CEIL	--	round to nearest integer greaterthan argument.

	REAL	->	REAL
*/

void c_ceil (void)
{
	genobj		* a;
	realobj		* r;

	if (! stack) {
		lerror ("CEIL", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("CEIL", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = ceil (((realobj *) a)->val);
		c_drop ();
		push (r);
	} else {
		lerror ("CEIL", ERR_WRTYPE, id2str (a->id));
	}
}




/*
	MOD	--	remainder.

	x	y	->	x % y.

	REAL	REAL	->	REAL
*/

void c_mod (void)
{
	genobj		* a, * b;
	realobj		* r;

	if (! stack || ! stack->next) {
		lerror ("MOD", ERR_2FEWARG);
		return;
	}

	if ((b = stack->obj)->id == REAL && (a = stack->next->obj)->id == REAL) {
		double	x = ((realobj *) a)->val,
			y = ((realobj *) b)->val;

		if (!(r = mallocobj (REAL)))
		{
			lerror ("MOD", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = x - y * floor (x / y);
		c_drop ();
		c_drop ();
		push (r);
	} else {
		if (a->id != REAL) lerror ("MOD", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("MOD", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	MAX	--	returns larger of to numbers.

	x	y	->	x % y.

	REAL	REAL	->	REAL
*/

void c_max (void)
{
	genobj		* a, * b;
	realobj		* r;

	if (! stack || ! stack->next) {
		lerror ("MAX", ERR_2FEWARG);
		return;
	}

	if ((b = stack->obj)->id == REAL && (a = stack->next->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("MAX", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = max (((realobj *) a)->val, ((realobj *) b)->val);
		c_drop ();
		c_drop ();
		push (r);
	} else {
		if (a->id != REAL) lerror ("MAX", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("MAX", ERR_WRTYPE, id2str (b->id));
	}
}


/*
	MIN	--	returns smaller of two numbers.

	x	y	->	x % y.

	REAL	REAL	->	REAL
*/

void c_min (void)
{
	genobj		* a, * b;
	realobj		* r;

	if (! stack || ! stack->next) {
		lerror ("MIN", ERR_2FEWARG);
		return;
	}

	if ((b = stack->obj)->id == REAL && (a = stack->next->obj)->id == REAL) {
		if (!(r = mallocobj (REAL)))
		{
			lerror ("MIN", ERR_NOMEM);
			return;
		}
		((realobj *)r)->val = min (((realobj *) a)->val, ((realobj *) b)->val);
		c_drop ();
		c_drop ();
		push (r);
	} else {
		if (a->id != REAL) lerror ("MIN", ERR_WRTYPE, id2str (a->id));
		if (b->id != REAL) lerror ("MIN", ERR_WRTYPE, id2str (b->id));
	}
}
