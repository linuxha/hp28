/****************************************************************

Module:
	Parser

Description:
	Command line parser


Modification history:

0.0	hjp	89-06-26

	initial version

0.1	hjp	89-07-25

	Type STRING added

0.2	hjp	89-08-28

	Size of strings fixed.

0.3	hjp	89-11-12

	Workaround for bug in sscanf.

0.4	hjp	89-11-23

	Binary objects added.

0.5	hjp	90-02-26

	Comment objects added.

0.6	hjp	90-03-02

	Lists added to parser.

0.7	hjp	90-03-03

	malloc replaced by mallocobj where possible (at last!).

0.8	hjp	90-03-07

	parsechar () now accepts '\0' as end of string.

0.9	njc	12-09-03

	Replaced the fgets in edit() with readline ( -DREADLINE )
****************************************************************/

#ifdef __TURBOC__
#include <alloc.h>
#else
#include <malloc.h>
#endif

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "errors.h"
#include "globvar.h"
#include "intcmd.h"
#include "misccmd.h"
#include "parser.h"
#include "rpl.h"

#ifdef READLINE
#include <readline/readline.h>
#include <readline/history.h>

#include <stdlib.h>		// needed by malloc
#endif

extern unsigned long int strtoul(const char *nptr, char **endptr, int base);

#ifdef READLINE
char	*cmdline;		/*  command line  (malloc'd for realine) */
char	empty  = 1;		/* is it empty	*/
char	*rdptr;			/* rdptr points to the first character not yet read by getobj	*/
char	pbuffer[PROGMAXSIZE];	/* buffer for building programs */
#else
char	cmdline [PROGMAXSIZE]	= "",		/* The command line     */
	empty			= 1,		/* is it empty	*/
	* rdptr 		= cmdline,	/* rdptr points to the first character not yet read by getobj	*/

	pbuffer [PROGMAXSIZE]; 			/* buffer for building programs */
#endif
/*
	parsechar:	return the next character of string given in C syntax
			and advance * pp after the character read.
*/

/* #ifndef uchar
typedef unsigned char uchar;
#endif

parsechar (uchar **pp){
*/

int
parsechar (char **pp){
	char 	* p = * pp;
	int	v;

	if (* p == '\\') {
		if (isdigit (* ++ p)) {
			v = * p - '0';
			while (isdigit (* ++ p)) {
				v = v * 8 + * p - '0';
			}
		} else {
			switch (* p) {
			case '"':
				v = * p;
				break;
			case 'n':
				v = '\n';
				break;
			case 'b':
				v = '\b';
				break;
			case 'f':
				v = '\f';
				break;
			case 'r':
				v = '\r';
				break;
			case '\\':
				v = '\\';
				break;
			case 'x':
				v = 0;
				while (isxdigit (* ++ p)) {
					v = v * 16 +
					    (* p > '9' ?
						toupper (*p)  - ('A' - 10):
						* p - '0'
					    );
				}
				p --;
				break;
			default:
				v = -2;
			}
			p ++;
                }
		* pp = p;
		return v;
	} else {
		* pp = p + 1;
		return (* p == '"' || * p == 0) ? EOF : * p;
	}
}


void	findwhite (void) {
	while (! isspace (* rdptr) && * rdptr != ',' && * rdptr) rdptr ++;
}

void	skipwhite (void) {
	while (isspace (* rdptr) || * rdptr == ',') rdptr ++;
}

genobj 
*getobj (void) {
	int	rc;
	int	i;
	static
#ifdef __TURBOC__
	long double	real;	/*	double would be enough, but sscanf does not work right with doubles < 1e-44	*/
#else
	double	real;
#endif
	static
	complex comp;
	char	* s0;
	genobj	* p;

	skipwhite ();
	if ( !(*rdptr) ) return NULL;				/* empty line	*/

#ifdef __TURBOC__
	if (sscanf (rdptr, "%Lf", &real) == 1) {

		findwhite ();

		p = mallocobj (REAL);
		((realobj *)p)->val =				/*	workaround for sscanf bug	*/
		    (real < - HUGE_VAL) ? - HUGE_VAL :
		    (real > + HUGE_VAL) ? + HUGE_VAL :
					  real;
#else
	if (sscanf (rdptr, "%lf", &real) == 1) {

		findwhite ();

		p = mallocobj (REAL);
		((realobj *)p)->val = real;
#endif
	} else if (* rdptr == '(') {

		rdptr ++;
		skipwhite ();
#ifdef __TURBOC__
		rc = (sscanf (rdptr, "%Lf", &real) == 1);
		comp.x =
		    (real < - HUGE_VAL) ? - HUGE_VAL :
		    (real > + HUGE_VAL) ? + HUGE_VAL :
					  real;
		findwhite ();
		skipwhite ();
		rc &= (sscanf (rdptr, "%Lf", &real) == 1);
		comp.y =
		    (real < - HUGE_VAL) ? - HUGE_VAL :
		    (real > + HUGE_VAL) ? + HUGE_VAL :
					  real;
#else
		rc = (sscanf (rdptr, "%lf", &real) == 1);
		comp.x = real;
		findwhite ();
		skipwhite ();
		rc &= (sscanf (rdptr, "%lf", &real) == 1);
		comp.y = real;
#endif
		findwhite ();
		skipwhite ();
		if (* rdptr == ')') rdptr ++;

		if (rc) {
			p = mallocobj (COMPLEX);
			((complexobj *)p)->val = comp;
		} else {
			p = NULL;	/* syntax lerror */
		}

	} else if (* rdptr == '\'') {

		rdptr ++;
		s0 = rdptr;
		findwhite ();
		if (rdptr [-1] == '\'') {
			rdptr [-1] = 0;
		}
		if (* rdptr) {
			* rdptr = 0;
			rdptr ++;
		}

//		if(p = mallocobj (QNAME)) { 	// BAD
		p = mallocobj(QNAME);	// BAD
		if(p) { 	// BAD
		  strncpy (((nameobj *) p)->name, s0, NAMELEN);
		  ((nameobj *) p)->name [NAMELEN - 1] = 0;
		} else {
		  lerror ("getobj", ERR_NOMEM, NULL);
		}

	} else if (* rdptr == '"') {
		int	size = sizeof (genobj),		/*	size of the string object	*/
			c;
		char	* cp;

		rdptr ++;
//		if (p = malloc (PROGMAXSIZE)) {		/*	BAD, malloc maximum size	*/
		p = malloc (PROGMAXSIZE);		/*	BAD, malloc maximum size	*/
		if( p ) {		/*	BAD, malloc maximum size	*/
			p->id = STRING;
			p->link = 0;
			cp = ((stringobj *) p)->val;
			while ((c = parsechar (& rdptr)) != EOF && size < PROGMAXSIZE) {
				* cp ++ = c;
				size ++;
			}
			* cp = 0; size ++;
			p->size = size;
			p = realloc (p, size);		/*	shrink stringobj too actually needed size	*/
		} else {
			lerror ("getobj", ERR_NOMEM);
		}

	} else if (* rdptr == '#') {

	  //long	l;
		unsigned long int	l;
		char	* last;

		s0 = rdptr + 1;
		findwhite ();
		last = rdptr - 1;
		if (* rdptr) {
			* rdptr = 0;
			rdptr ++;
		}

		switch( *last) {
		case 'b':
			l = strtoul (s0, &s0, 2);
			break;
		case 'o':
			l = strtoul (s0, &s0, 8);
			break;
		case 'd':
			l = strtoul (s0, &s0, 10);
			break;
		case 'h':
			l = strtoul (s0, &s0, 16);
			break;
		default:
			l = strtoul (s0, &s0, radix);
			break;
		}

		if (s0 >= last)		/*	Number was successfully converted	*/
		{
//			if (p = mallocobj (BINARY)) {
			p = mallocobj (BINARY);
			if( p ) {
				((binaryobj *)p)->val	= l;
			} else {
				lerror ("getobj", ERR_NOMEM, NULL);
			}
		} else {
			p = NULL;	/*	syntax lerror	*/
		}

	} else if (*rdptr == ';') {	/*	start of a comment	*/

		int	size = sizeof (genobj);		/*	size of the comment object	*/
		char	* cp;

//		if (p = malloc (PROGMAXSIZE)) {		/*	malloc maximum size	*/
		p = malloc (PROGMAXSIZE);		/*	malloc maximum size	*/
		if( p ) {
			p->id = COMMENT;
			p->link = 0;
			cp = ((commentobj *) p)->val;
			while (* ++ rdptr && * rdptr != '\n' && size < PROGMAXSIZE) {
				* cp ++ = * rdptr;
				size ++;
			}
			* cp = 0; size ++;
			p->size = size;
			p = realloc (p, size);		/*	shrink commentobj to actually needed size	*/

			if (* rdptr) {
				* rdptr = 0;
				rdptr ++;
			}
		} else {
			lerror ("getobj", ERR_NOMEM, NULL);
		}

	} else if (* rdptr == '{') {	/*	start of a list	*/

		listobj	* head = NULL,
			* p,
			* pl = NULL;
		genobj	* obj;

		rdptr ++;
		do {
  //			if (! (p = mallocobj (LIST))) {
			p = mallocobj(LIST);
			if( !p ) {
				lerror ("getobj", ERR_NOMEM, NULL);
				return NULL;
			} else if (! head) {
				head = p;
			} else {
				p->link ++;
			}
			if (pl) {
				pl->next = p;
			}
                        obj = getobj ();
			if (obj->id == OP && ((opobj *)obj)->fptr == c_listend) {
				obj = NULL;
			}

			if (obj) {
				obj->link ++;
			}
			p->obj = obj;
			pl = p;
		} while (obj);
		return (genobj *) head;
        } else {
		s0 = rdptr;
		findwhite ();
		if (* rdptr) {
			* rdptr = 0;
			rdptr ++;
		}
		for (i = 0; i < NOOPS && strcmp (s0, ops[i].name); i ++);
		if (i < NOOPS) {
			if (ops [i].fptr == c_pbegin) {	/* start of a program block	*/

				genobj	** pp;
				genobj	* obj;

				p = malloc (PROGMAXSIZE);
				if (p == NULL) {
					lerror ("getobj", ERR_NOMEM, NULL);
					return NULL;
				}
				p->id = PROGRAM;
				p->link = 0;
				pp = (genobj **)(p + 1);
				do {
					obj = getobj ();
					if (obj == NULL) {
						obj = ops + i + 1;	/* ops + i is  "<<"  ==> ops + i + 1 is ">>"    */
					}

					* pp ++ = obj;
					obj->link ++;
				} while (obj != ops + i + 1);
				p->size = (char *) pp - (char *) p;
				p = realloc (p, p->size);
			} else {
				p = ops + i;
			}
		} else {		/* not in command list ==> unquoted name	*/
  //			if (p = mallocobj (UNAME)) {
			p = mallocobj (UNAME);
			if( p ) {
				strncpy (((nameobj *) p)->name, s0, NAMELEN);
				((nameobj *) p)->name [NAMELEN - 1] = 0;
			} else {
			    fprintf(stderr, "parse malloc error (%s/%s)\n", ((nameobj *) p)->name, s0);
			}
		}
	}
	return p;
}

const char prompt[] = "===> ";

void
edit(genobj * obj) {
#ifdef READLINE
  /*
  ** I'd like to get readline added here so I can use cmd history etal
  */
  if(cmdline != NULL) {
    free(cmdline);		// free cmdline before use
  }

  // line returned is allocated with malloc(3); the caller must free it when
  // finished.  The line returned has the final newline  removed,  so  only
  // the text of the line remains.
  if(!(cmdline = readline(prompt))) {	//
    // NULL returned
    // turn off
    printf("\n");
    c_off();
  }	
  // Add input to history.
  add_history(cmdline);
 #else
       //printf ("===> ");
	printf(prompt);
	//gets (cmdline);	// warning: 'gets' is dangerous and should not be used.
	if(!(fgets(cmdline, sizeof(cmdline), stdin))) {
	  // NULL returned
	  // turn off
	  printf("\n");
	  c_off();
	}	
	//fgets (cmdline, 256, STDIN);
#endif
	rdptr = cmdline;
	empty = *cmdline == 0;
}

void
*readvalue () {
  if (empty) {
    edit (NULL);
  }

  return getobj ();
}

