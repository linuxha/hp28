/****************************************************************

	Internally used commands

0.0	hjp	89-06-26

	initial version

0.1	hjp	89-09-03

	findvar added

0.2	hjp	89-12-02

	mallocobj added.

0.3	hjp	89-12-11

	fpehandler added.

0.4	hjp	90-03-07

	inthandler added.

****************************************************************/

#ifndef I_intcmnds

	#define I_intcmnds

	void		push (genobj * p);
	void		destroy (genobj * p, int level);
	genobj *	duplicate (genobj * obj, int level);
	void		fpehandler (int sig);
	void		inthandler (int sig);
	genobj *	mallocobj (int type);
	void		printobj (genobj * obj);
	void		psr (listobj * l, int n);
	void		printstack (void);
	void		interprete (genobj * obj, int level);
	varobj *	findvar (char * name);

	extern
	char	iobuffer [],
		* iop,
		* ioend;

#endif
