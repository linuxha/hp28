/****************************************************************

Module:
	FileCmd

Description:
	Commands for file I/O


Modification history:

	0.0	hjp	89-08-14

		initial version.

	0.1	hjp	89-08-15

		SAVE	debugged
		LOAD	added
		SYSTEM	added.

	0.2	hjp	89-11-23

		SAVE now _appends_ to file.

	0.3	hjp	89-12-02

		PRINT added.

	0.4	hjp	89-12-11

		minor bug fixing.

****************************************************************/

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <malloc.h>		// ??? I thought stdlib.h took care of this?

#include "rpl.h"
#include "errors.h"
#include "filecmd.h"
#include "globvar.h"
#include "intcmd.h"
#include "stackcmd.h"

/*
** This function will allow me to substitute common shell-isms
** into the path of the LOAD and SAVE commands. Such as:
**
** ~
** ~root
** ${HOME}
**
*/
FILE
*ffopen(const char *path, const char *mode) {
  FILE *fp;
  //char *filepath;

  fp = fopen(path, "r");

  if (fp) {
    c_drop ();					/*	drop name			*/
  }

  return(fp);
}

/*
	SAVE: save object at level 2 to file at level 1

	1: obj	2: string	->
*/

void c_save (void) {
  listobj * a, * b;
  FILE	* fp;

  //printf("> Save:\n");

  if ((b = stack) && (a = stack->next)) {
    if (b->obj->id != STRING) {
      lerror ("SAVE", ERR_WRTYPE, id2str (b->obj->id));
      //eprintf(estr[ERR_WRTYPE], "SAVE", id2str(b->obj->id));
    } else if ((fp = fopen (((stringobj *) b->obj)->val, "a"))) {

      c_drop ();		/*	drop name	*/

      iop = iobuffer;
      printobj (a->obj);
      fprintf (fp, "%s\n", iobuffer);

      if (fclose (fp)) {
	lerror ("SAVE", ERR_DOS, strerror (errno));
	//eprintf(estr[ERR_DOS], "SAVE", strerror (errno));
      }
      c_drop ();	/*	drop saved object	*/

    } else {
      lerror ("SAVE", ERR_DOS, strerror (errno));
      //eprintf(estr[ERR_DOS], "SAVE", strerror (errno));
    }
  } else {
    lerror ("SAVE", ERR_2FEWARG, NULL);
    //eprintf(estr[ERR_2FEWARG], "SAVE");
  }
}


/*
  LOAD: load file into command line

  1: string	->	??

  Ex:

    "/home/njc/.hp28/math.rpl" LOAD
*/

//extern void *malloc(size_t size);
//extern void free(void *ptr);

// -----------------------------------------------------------------------------
// Okay here's where I'd like to add the ability to access the Unix env a
// little better. I want to be to use ENV variables so I can type things like:
// "${HOME}/dev/shell/math.rpl" LOAD
// -----------------------------------------------------------------------------
void c_load (void) {
  listobj * b;
  FILE	  * fp;
  int	  rdcnt;

  if ((b = stack)) {
    if (b->obj->id != STRING) {

      lerror ("LOAD", ERR_WRTYPE, id2str (b->obj->id));
      //eprintf(estr[ERR_WRTYPE], "LOAD", id2str(b->obj->id));
    } else {
      // I'd like to add support for ENV variables in the name like ~ and ${HOME}

      fp = fopen (((stringobj *) b->obj)->val, "r");

      if (fp) {
	c_drop ();					/*	drop name			*/

	if(cmdline != NULL) {
	  free(cmdline);
	}
	// PROGMAXSIZE is about 4K if we push to 32K we should be safe for a while
	// @FIXME: Not reading entire file if program is larger than PROGMAXSIZE
#define READSIZE	PROGMAXSIZE * 8
	if(!(cmdline = (char *) malloc(sizeof(char) * READSIZE))) {
	  lerror ("LOAD", ERR_DOS, strerror (errno));
	}
	rdcnt = fread (cmdline, 1, READSIZE - 1, fp);   /*	try to read max. program size	*/
	cmdline [rdcnt] = 0;				/*	append EOS			*/
	rdptr = cmdline; empty = 0;			/*	simulate edit ()		*/

	if (fclose (fp)) {
	  lerror ("LOAD", ERR_DOS, strerror(errno));
	}
      } else {
	//lerror ("LOAD", ERR_DOS, strerror (errno));
	eprintf(estr[ERR_FILE], "LOAD", ((stringobj *) b->obj)->val, strerror (errno));
      }
    }

  } else {
    lerror ("LOAD", ERR_2FEWARG, NULL);
    //eprintf(estr[ERR_2FEWARG], "LOAD");
  }
}

/*
r
"foo.rpl"
LOAD
*/

extern int system(const char *command);

void c_system (void) {
  listobj	* b;

  if ((b = stack)) {

    if (b->obj->id != STRING) {

      lerror ("SYSTEM", ERR_WRTYPE, id2str (b->obj->id));
      //eprintf(estr[ERR_WRTYPE], "SAVE", id2str(b->obj->id));
    } else if (system (((stringobj *) b->obj)->val)) {

      lerror ("SYSTEM", ERR_DOS, strerror (errno));
      //eprintf(estr[ERR_DOS], "SAVE", strerror (errno));
    }
    c_drop ();

  } else {

    lerror ("SYSTEM", ERR_2FEWARG, NULL);
  }
}


/*
  PRINT: write object at level 1 to stdout

  1: obj	->
*/

void c_print (void) {
  listobj	* a;

  a = stack;

  if (a) {
    iop = iobuffer;
    printobj (a->obj);
    printf ("%s\n", iobuffer);
    c_drop ();	/*	drop printed object	*/

  } else {
    lerror ("PRINT", ERR_2FEWARG, NULL);
  }
}

void c_dump(void) {
  listobj *a;

  a = stack;

  if (a) {
    iop = iobuffer;
    printobj (a->obj);
    printf ("(%d) %s\n", a->id, iobuffer);
    c_drop ();	/*	drop printed object	*/

  } else {
    lerror ("PRINT", ERR_2FEWARG, NULL);
  }
}

