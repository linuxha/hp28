/****************************************************************

Module:
	StackCmd

Description:
	Commands for manipulating the stack


Modification history:

	0.0	hjp	89-06-26

		initial version: DROP, SWAP, CLEAR

	0.1	hjp	89-06-26

		DUP added

****************************************************************/

#include <stddef.h>

#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "intcmd.h"
#include "stackcmd.h"
#include "debug.h"

/*
	drop the element at the top of the stack
*/

void c_drop (void)
{
	listobj * l;

	l = stack;

	if (l) {
		if (l->id == LIST) {
			stack = l->next;
			destroy ((genobj *)l, 0);
		} else {
			lerror ("drop", INT_STKNOLIST, NULL);
		}
	} else {
		lerror ("drop", ERR_STKEMPTY, NULL);
	}
}


/*
	swap two topmost arguments
*/

void c_swap (void)
{
	listobj * a, * b;

	if ((a = stack) && (b = stack->next)) {
		a->next = b->next;
		b->next = a;
		stack = b;
	} else {
		lerror ("swap", ERR_2FEWARG, NULL);
	}
}


/*
	clear stack
*/
void c_clear (void)
{
	while (stack) {
		c_drop ();
	}
}

/*
	duplicate topmost element
*/

void c_dup (void)
{
#ifdef TRACE
	printf ("c_dup () {\n");
#endif
	if (stack) {
		push (stack->obj);
	} else {
		lerror ("DUP", ERR_2FEWARG);
	}
#ifdef TRACE
	printf ("} c_dup\n");
#endif
}
