/****************************************************************

	Typedefs and constants

0.0	hjp	89-06-14

	initial version

0.1	hjp	89-07-08

	function prototypes moved to intcmds.h

0.2	hjp	89-07-25

	stringobj added

0.3	hjp	89-08-29

	loopobj added

0.4	hjp	89-09-04

	link count added to all objects.

0.5	hjp	89-11-23

	binary object added.

0.6	hjp	89-12-02

	object types changed to be continuous.
	macro t22int (convert 2 types to int) added.

0.7	hjp	89-12-02

	Comment object added.

0.8	hjp	90-03-06

	port.h added.

****************************************************************/


#ifndef I_hp

	#define I_hp

	#include <hjp.h>
	#include <math.h>
	#include <port.h>

	#define	PROGMAXSIZE	4096
	#define NAMELEN		32

	#define NOOBJ	-1
	#define REAL	1
	#define COMPLEX	2
	#define STRING	3
	#define RVECT   4
	#define RMAT    5
	#define CVECT   6
	#define CMAT    7
	#define LIST	8
	#define QNAME	9
	#define UNAME	10
	#define OP	11
	#define PROGRAM	12
	#define BINARY	13

	#define VARIABLE 16		/*	internal use only	*/
	#define START	17		/*	internal use only	*/
	#define FOR	18		/*	internal use only	*/
	#define DO	19		/*	internal use only	*/
	#define WHILE	20		/*	internal use only	*/
	#define COMMENT	21		/*	internal use only	*/

	#define t22int(a, b)	(((a)<<8)|(b))


	typedef struct complex
		complex;

	typedef struct genobj {
		int	id;
		uint	link;
		uint	size;
	} genobj;

	typedef struct realobj {
		int	id;
		uint	link;
		uint	size;
		double	val;
	} realobj;

	typedef struct complexobj {
		int	id;
		uint	link;
		uint	size;
		complex	val;
	} complexobj;

	typedef struct opobj {
		int	id;
		uint	link;
		uint	size;
		void	(* fptr)(void);
		char	name [32];
	} opobj;

	typedef struct listobj {
	  int	 id;			// What type of Object it is
	  uint	 link;
	  uint	 size;
	  genobj *obj;

	  struct listobj
	         *next;
	} listobj;

	typedef struct nameobj {
		int	id;
		uint	link;
		uint	size;
		char	name [NAMELEN];
	} nameobj;

	typedef struct varobj {
		int	id;
		uint	link;
		uint	size;
		char	name [NAMELEN];
		genobj	* val;
		struct varobj
			* next;
	} varobj;

	typedef struct stringobj {
		int	id;
		uint	link;
		uint	size;
		char	val [1];	/* dummy to calm the compiler.							*/
					/* The string can be up to 65521 chars long (including '\0')			*/
	} stringobj;

	typedef struct loopobj {
		int	id;
		uint	link;
		uint	size;
		struct
		loopobj	*next;
		genobj	**addr;
		double	cnt;
		varobj  *var;
	} loopobj;


	typedef struct binaryobj {
		int	id;
		uint	link;
		uint	size;
		long	val;
	} binaryobj;

	typedef stringobj commentobj;

#endif
