/****************************************************************

Module:
	StoreCmd

Description:
	Commands for manipulating variables


Modification history:

0.0	hjp	89-07-08

	initial version.

0.1	hjp:	89-07-25

	comments added.

0.2	hjp:	89-08-14

	PURGE and USER added.

0.3	hjp:	89-11-08

	STO now globbers existing variables.

0.4	hjp:	89-11-23

	PURGE now can delete files.
	(This would belong into FileCmd, but I didn't want 2 purges.)

0.5	hjp	90-03-03

	malloc replaced by mallocobj (at last!).

****************************************************************/

#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "rpl.h"
#include "errors.h"
#include "intcmd.h"
#include "stackcmd.h"
#include "storecmd.h"
#include "globvar.h"
#include "debug.h"

/*
	STO: store object in variable

	2: obj	1: qname	->
*/

void	c_sto	(void)
{
	listobj * a, * b;
	varobj	* p;

	if ((b = stack) && (a = stack->next)) {

		if (b->obj->id != QNAME) {
		    fprintf(stderr, "ERR_WRTYPE: %s\n", ((nameobj *) b->obj)->name);
			lerror ("STO", ERR_WRTYPE, NULL);
		} else if (p = findvar (((nameobj *) b->obj)->name)) {
		    //fprintf(stderr, "Already exists: %s\n", ((nameobj *) b->obj)->name);
			destroy (p->val, 1);	/*	destroy old contents of variable	*/

			c_drop ();		/*	drop name	*/

			p->val = a->obj;

			stack = a->next;	/*	drop stored object w/o destroing it !!	*/
			a->obj = NULL;
			destroy (a, 0);

			//fprintf(stderr, "Already exists done: %s\n", ((nameobj *) b->obj)->name);
		} else if (p = mallocobj (VARIABLE)) {
		    //fprintf(stderr, "New: %s\n", ((nameobj *) b->obj)->name);

			p->id = VARIABLE;
			p->link = 1;
			p->size = sizeof (varobj);
			strcpy (p->name, ((nameobj *) b->obj)->name);

			c_drop ();		/*	drop name	*/

			p->val = a->obj;

			stack = a->next;	/*	drop stored object w/o destroing it !!	*/
			a->obj = NULL;
			destroy (a, 0);

			p->next = vars; 	/*	hook it into variable list	*/
			vars = p;

			//fprintf(stderr, "New done: %s\n", ((nameobj *) b->obj)->name);
		}
	} else {
	    fprintf(stderr, "ERR_2FEWARG: %s\n", ((nameobj *) b->obj)->name);
		lerror ("STO", ERR_2FEWARG, NULL);
	}
}

/*
	RCL: recall variable
	1: qname	->	1: obj
*/

void	c_rcl (void)
{
	nameobj * a;
	varobj	* p;

	if (! stack) {
		lerror ("RCL", ERR_2FEWARG, NULL);
		return;
	}

	if ((a = stack->obj)->id != QNAME) {
		lerror ("RCL", ERR_WRTYPE, NULL);
		return;
	}

	for (p = vars; p && strcmp (a->name, p->name); p = p->next);

	c_drop ();

	if (p) {
		push (p->val);
	} else {
		lerror ("RCL", ERR_NXVAR, NULL);
		return;
	}
}

/*
	SHOW: show system commands
*/
void	c_show (void) {
    // ops[n]
    // OP,	0,	sizeof (opobj), c_add,		"+",
    /*
        typedef struct opobj {
                int     id;
                uint    link;
                uint    size;
                void    (* fptr)(void);
                char    name [32];
        } opobj;
    */
    int i;
    for(i = 0; i < noops; i++) {
	printf("'%s'\n", ops[i].name);
    }
}

/*
	USER: show user variables
*/

void	c_user (void)
{
	varobj	* p;

	if (vars) {
		for (p = vars; p; p = p->next) {
			printf ("'%s'\n", p->name);
		}
	} else {
		lerror ("USER", ERR_NOVAR, NULL);
		return;
	}
}

/*
	PURGE: purge user variable(s) or file.

	1:qname	->
	1:v	->		(the variable with name v is purged)

	1:string	->
	1:s		->	(the file with name s is unlinked)
*/

void	c_purge (void)
{
	nameobj * a;
	varobj	* p, * pp;

	if (! stack) {
		lerror ("PURGE", ERR_2FEWARG, NULL);
		return;
	}

	if ((a = stack->obj)->id == QNAME) {

		for (pp = NULL, p = vars;
		     p && strcmp (a->name, p->name);
		     pp = p, p = p->next);

		c_drop ();

		if (p) {
			if (pp) {
				pp->next = p->next;
			} else {
				vars = p->next;
			}
			destroy (p, 1);
		} else {
			lerror ("PURGE", ERR_NXVAR, NULL);
			return;
		}
	} else if ((a = stack->obj)->id == STRING) {
		if (unlink (((stringobj *) a)->val) == -1) {
			lerror ("PURGE", ERR_DOS, strerror (errno));
		}
		c_drop ();
	} else {
		lerror ("PURGE", ERR_WRTYPE, NULL);
		return;
	}
}
