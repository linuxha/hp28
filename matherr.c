
/****************************************************************

Module:
	matherr

Description:
	Error handler for exceptions in math functions.


Modification history:

	0.0	hjp	89-12-17

		initial version.

****************************************************************/

#include <math.h>
#include <port.h>


int matherr(struct exception *e)
{
	if (e->type == UNDERFLOW)
	{
		/* flush underflow to 0 */
		e->retval = 0;
		return 1;
	}
	if (e->type == TLOSS)
	{
		/* total loss of precision, but ignore the problem */
		return 1;
	}
	if (e->type == OVERFLOW)
	{
		/* set overflow to HUGE_VAL */
		e->retval = HUGE_VAL;
		return 1;
	}
	/* all other errors are fatal */
	return 0;
}

