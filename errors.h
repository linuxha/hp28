/****************************************************************

	Variables and constants used for error-messages

0.0	hjp	89-06-14

	initial version

0.1	hjp	89-07-25

	ERR_NXVAR added

0.2	hjp	89-08-14

	ERR_NOVAR added

0.3	hjp	89-08-15

	ERR_DOS added

0.4	hjp	89-08-29

	ERR_LOOP added

0.5	hjp	89-10-05

	INT_BADLINK added.
	error () changed to variable arguments.

0.6	hjp	89-12-11

	ERR_FPE added.

****************************************************************/

#ifndef I_errors

#include <errno.h>
//extern int errno;

#define I_errors

extern char *errstr[];
extern char *estr[];

extern void lerror(const char *fmt, int errnum, ...);
extern void eprintf(const char *fmt, ...);

enum {
  ERR_NOERR,
  ERR_WRTYPE,
  ERR_STKEMPTY,
  ERR_2FEWARG,
  ERR_SYNTAX,
  ERR_NOMEM,
  ERR_NXVAR,
  ERR_NOVAR,
  ERR_DOS,			/*  OS Error */
  ERR_LOOP,
  ERR_FPE,
  ERR_FILE,

  INT_STKNOLIST,		/* internal errors	*/
  INT_NXOBJ,
  INT_BADLINK,

  INT_BUFOVER,			/* internal fatal errors	*/
};

char *id2str(int id);

#if defined ERRORS_C && defined STDARGBUG
void	lerror	();
#error "STDARGBUG";
#else
void	lerror(const char * function, int errnum, ...);
void    eprintf(const char *fmt, ...);
#endif

#endif
