# hp28

A command line interface hp28 like calculator. This was originally named rpl but I had (still have) an HP28 caculator and couldn't recall rpl so I renamed it.

This code was originally written by Peter J. Holzer. Since his email address no longer works (yes, I actually know how to read bang addressing) I've not been able to contact him. It retains his copyright and much of the code is his as released to the comp.sources.mis newgroup. I suspect that the BSD license will match that of what he originally posted so I'll probably use that here. More investigation is required.

## Author
hp@honey.tuwien.ac.at

...!mcsun!tuvie!asupa!honey!hp

hp@vmars.uucp

Peter J. Holzer

## Maintainer
Neil Cherry <ncherry@linuxha.com>

## RPL (on Wikipedia)
RPL (derived from Reverse Polish Lisp according to its original developers) is a handheld calculator operating system and application programming language used on Hewlett-Packard's scientific graphing RPN (Reverse Polish Notation) calculators of the HP 28, 48, 49 and 50 series, but it is also usable on non-RPN calculators, such as the 38, 39 and 40 series.

[RPL Wikipedia article](https://en.wikipedia.org/wiki/RPL_(programming_language))

## Objective

I've been using this calculator since March of 1990 on all my Linux/Unix workstations and servers (once you start with RPN it's a little hard to go back to non-RPN).

I've made a few minor modifications related to the file system and added support for readline and fixed a few minor issue with moving from older Unixes to new ones.

One important thing to note is that I haven't really spent a great deal of time cleaning up the compile time warnings.

## Usage

From the command line type hp28 and you should get the display (section between the dashes (replaced with equals in the example below)) and the prompt '===> '

```
$ hp28
=======================================
=======================================
===> 22 3 /
=======================================
1:	7.33333333333333
=======================================
===> 2 FIX
=======================================
1:	7.33
=======================================
===> PI
=======================================
1:	3.14159265358979
=======================================
===> OFF
$ 
```
See the rpl.doc for command syntax

## Todo

- Clean up the compile time warnings
- Document the new features
- make sure this thing actually works correctly.

## License

Portions of this code are under "Copyright (c) 1985 Regents of the University of California" which I think is the basis of the BSD license.
