/****************************************************************

	Functions used for implementing user logarithmic commands

0.0	hjp	89-12-03

	initial version
	R->C, C->R, RE, IM extracted from ArithCmd.

****************************************************************/

#ifndef I_cmplxcmd

	#define I_cmplxcmd

	void	c_c_r	(void);
	void	c_im	(void);
	void	c_r_c	(void);
	void	c_re	(void);

#endif
