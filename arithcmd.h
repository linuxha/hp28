/****************************************************************

	Functions used for implementing user arithmetic commands

0.0	hjp	89-06-27

	initial version

0.1	hjp	89-10-01

	EXP and LN added.

0.2	hjp	89-11-08

	PI, e and i added.
	SIN, ASIN, COS, ACOS, TAN, ATAN added.

0.3	hjp	89-11-15

	C->R, R->C, RE, IM added.

0.4	hjp	89-12-02

	LN and EXP moved to LogCmd
	SIN, TAN, COS, ASIN, ATAN, ACOS moved to TrigCmd
	R->C, C->R, RE, IM moved to CmplxCmd.

****************************************************************/

#ifndef I_arithcmd

	#define I_arithcmd

	void	c_add	(void);
	void	c_div	(void);
	void	c_e	(void);
	void	c_i	(void);
	void	c_inv	(void);
	void	c_mul	(void);
	void	c_neg	(void);
	void	c_pi	(void);
	void	c_pow	(void);
	void	c_sq	(void);
	void	c_sqrt	(void);
	void	c_sub	(void);

#endif
