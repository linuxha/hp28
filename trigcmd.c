/****************************************************************

Module:
	TrigCmd

Description:
	Commands related to trigonometry (The TRIG menu on HP28C)

Modification history:

0.0	hjp	89-12-03

	initial version
	SIN, COS, TAN, ASIN, ACOS, ATAN extracted from ArithCmd.

0.1	hjp	90-03-03

	constant complex_one moved to globvar.c.

0.2	hjp	90-03-07

	minor bug fixes.

****************************************************************/

#include "arithcmd.h"
#include "errors.h"
#include "globvar.h"
#include "rpl.h"
#include "intcmd.h"
#include "logcmd.h"
#include "stackcmd.h"
#include "trigcmd.h"

/*
	SIN	compute sine of object in level 1

	x	->	sin (x)

	real	->	real
	complex ->	complex
*/

void c_sin (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("SIN", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
//		if (!(c = mallocobj (REAL)))
		c = (realobj *) mallocobj (REAL);
		if ( !c ) {
			lerror ("SIN", ERR_NOMEM);
			return;
		}
		c->val = sin (((realobj *) a)->val);
		c_drop ();
		push ((genobj *) c);
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		{
			lerror ("SIN", ERR_NOMEM);
			return;
		}
		b->val.x = sin (x) * cosh (y);
		b->val.y = cos (x) * sinh (y);
		c_drop ();
		push (b);
	} else {
		lerror ("SIN", ERR_WRTYPE);
	}
}

/*
	COS	compute cosine of object in level 1

	x	->	cos (x)

	real	->	real
	complex ->	complex
*/

void c_cos (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("COS", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("COS", ERR_NOMEM);
			return;
		}
		c->val = cos (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		{
			lerror ("COS", ERR_NOMEM);
			return;
		}
		b->val.x = cos (x) * cosh (y);
		b->val.y = sin (x) * sinh (y);
		c_drop ();
		push (b);
	} else {
		lerror ("COS", ERR_WRTYPE);
	}
}

/*
	TAN	compute tangent of object in level 1

	x	->	tan (x)

	real	->	real
	complex ->	complex
*/

void c_tan (void)
{
	genobj		* a;
	realobj 	* c;
	complexobj	* b;

	if (! stack) {
		lerror ("TAN", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("TAN", ERR_NOMEM);
			return;
		}
		c->val = tan (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		double	x = ((complexobj *) a)->val.x,
			y = ((complexobj *) a)->val.y;

		if (! (b = mallocobj (COMPLEX)))
		{
			lerror ("TAN", ERR_NOMEM);
			return;
		}
		b->val.x = sin (x) * cos (x) / (sinh (y) * sinh (y) + cos (x) * cos (x));
		b->val.y = sinh (y) * cosh (y) / (sinh (y) * sinh (y) + cos (x) * cos (x));
		c_drop ();
		push (b);
	} else {
		lerror ("TAN", ERR_WRTYPE);
	}
}


/*
	ATAN	compute arcus tangent of object in level 1

	x	->	arc tan (x)

	real	->	real
	complex ->	complex
*/

void c_atan (void)
{
	genobj		* a;
	realobj 	* c;

	if (! stack) {
		lerror ("ATAN", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("ATAN", ERR_NOMEM);
			return;
		}
		c->val = atan (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		static complexobj	z = { COMPLEX, 1, sizeof (complexobj), 0, 0 };

		z.val = ((complexobj *)a)->val;

		c_drop ();

		/*	compute atan (z) = -i * ln ((1 + i*z)/sqrt(1+z^2))
			(In the manual the wrong formula
				i / z * ln ((i + z)/(i - z))
			is given
		*/

		c_i (); c_neg ();
		push (&complex_one);
		c_i ();
		push (&z);
		c_mul ();
		c_add ();
		push (&complex_one);
		push (&z);
		c_sq ();
		c_add ();
		c_sqrt ();
		c_div ();
		c_ln ();
		c_mul ();

	} else {
		lerror ("ATAN", ERR_WRTYPE);
	}
}


/*
	ACOS	compute arcus cosine of object in level 1

	x	->	arc cos (x)

	real	->	real
	complex ->	complex
*/

void c_acos (void)
{
	genobj		* a;
	realobj 	* c;

	if (! stack) {
		lerror ("ACOS", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("ACOS", ERR_NOMEM);
			return;
		}
		c->val = acos (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		static complexobj	z = { COMPLEX, 1, sizeof (complexobj), 0, 0 };

		z.val = ((complexobj *)a)->val;

		c_drop ();

		/*	compute acos (z) = -i ln (z + sqrt (z*z - 1))	*/

		c_i (); c_neg (); push (&z);
		push (&z); push (&z); c_mul (); push (&complex_one); c_sub (); c_sqrt (); c_add ();
		c_ln (); c_mul ();

	} else {
		lerror ("ACOS", ERR_WRTYPE);
	}
}


/*
	ASIN	compute arcus sine of object in level 1

	x	->	arc sin (x)

	real	->	real
	complex ->	complex
*/

void c_asin (void)
{
	genobj		* a;
	realobj 	* c;

	if (! stack) {
		lerror ("ASIN", ERR_2FEWARG);
		return;
	}

	if ((a = stack->obj)->id == REAL) {
		if (!(c = mallocobj (REAL)))
		{
			lerror ("ASIN", ERR_NOMEM);
			return;
		}
		c->val = asin (((realobj *) a)->val);
		c_drop ();
		push (c);
	} else if (a->id == COMPLEX) {
		static complexobj	z = { COMPLEX, 1, sizeof (complexobj), 0, 0 };

		z.val = ((complexobj *)a)->val;

		c_drop ();

		/*	compute asin (z) = -i ln (i*z + sqrt (1 - z*z)) */

		c_i ();
		c_neg ();
		c_i ();
		push (&z);
		c_mul ();
		push (&complex_one);
		push (&z);
		push (&z);
		c_mul ();
		c_sub ();
		c_sqrt ();
		c_add ();
		c_ln ();
		c_mul ();

	} else {
		lerror ("ASIN", ERR_WRTYPE);
	}
}
