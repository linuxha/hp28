/****************************************************************

	BinCmd	--	Commands for manipulating binary objects.
			(The BINARY menu on HP28)

0.0	hjp	90-03-03

	initial version

****************************************************************/

#include "bincmd.h"
#include "errors.h"
#include "globvar.h"
#include "intcmd.h"
#include "rpl.h"
#include "stackcmd.h"

/*
	B->R	convert binary to real

	BINARY ->	REAL
*/

void	c_b_r (void)
{
	realobj 	* a;
	binaryobj	* c;

	if (! stack) {
		lerror ("B->R", ERR_2FEWARG);
		return;
	}

	if ((c = stack->obj)->id == BINARY) {

		if (!(a = mallocobj (REAL)))
		{
			lerror ("B->R", ERR_NOMEM);
			return;
		}
		a->val = c->val;
		c_drop ();
		push (a);
	} else {
		lerror ("B->R", ERR_WRTYPE);
	}
}


/*
	R->B	convert real ro binary

	REAL	->	BINARY
*/

void	c_r_b (void)
{
	realobj 	* c;
	binaryobj	* a;

	if (! stack) {
		lerror ("R->B", ERR_2FEWARG);
		return;
	}

	if ((c = stack->obj)->id == REAL) {

		if (!(a = mallocobj (BINARY)))
		{
			lerror ("R->B", ERR_NOMEM);
			return;
		}
		a->val = c->val;
		c_drop ();
		push (a);
	} else {
		lerror ("R->B", ERR_WRTYPE);
	}
}
