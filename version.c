#include <stdio.h>

#ifndef __MYVERSION__
// Major.minor.patch
// Major - Major change that affect backward compatibility
// minor - Added feature that doesn't affect backward compatibility
// patch - Small changes that are not features (fixed puctuation, char type, bug fix)
//                    "M.m.p"
#define __MYVERSION__ "2.1.10"
#endif

void c_version() {
    printf("HP28 %s, Compiled on: %s %s\n", __MYVERSION__, __DATE__, __TIME__);
}
